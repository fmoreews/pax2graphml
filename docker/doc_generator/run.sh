#!/bin/bash
bash merge-md.sh
IMAGENAME=docgenerator
docker run  -it --rm  -v $PWD:/work  -w /work $IMAGENAME  bash generate.sh

mv p2g-getting-started.pdf ../../p2g-getting-started.pdf
