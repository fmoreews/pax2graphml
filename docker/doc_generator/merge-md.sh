
TFILE="p2g-getting-started.md"


function init {
  echo "" > $TFILE
}

function text {
  echo "$1" >> $TFILE
  echo "" >> $TFILE
}

function add {
   cat "$1" >> $TFILE
   echo "" >> $TFILE
}

init
text "![Logo-long.png](template/Logo-long.png)"
text "<br />"


#text "**Table of content**"
#text  "Getting started with Pax2Graphml"
add ../../README.md 
#text  "Virtual Machine "
add ../../vm.md 
#text  "Ubuntu installation"
add ../../ubuntu.md 
#text  "Docker installation "
add ../../docker/pax2graphml/README.md 
#text  " Quick start and tutorials"
add ../../first-step/README.md 
#text "tutorials presentation"
add ../../docker/pax2graphml/data/README.md

text  " The logging system "
add ../../logging.md 
text "<br/> <br/><br/> document automatically generated from the markdown pages "
# ../../install_curl.md 





