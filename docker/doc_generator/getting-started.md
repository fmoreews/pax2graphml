![Logo-long.png](template/Logo-long.png)

<br />

# PAX2GraphML 

## Features

The package PAX2GraphML  allows to manipulate   BioPAX data sources transformed as GRAPHML files.

It is expecially design  around the SPAIM model (Substrate, Product, Activator, Inhibitor, Modulator). 
In this model, reactions and biological entities are represented as nodes. The edges hold the roles of the entities (is_substrat_of, is_activator_of...)
Among other things, it allows to visualize and analyze the graphs, to extract sub-components and to build an influence graph.

Input graphs are in .graphml format, assumed under the SPAIM model . 
## Documentation 
The python code documentation is available on [https://fjrmoreews.github.io/pax2graphml/](https://fjrmoreews.github.io/pax2graphml/)
## Data Sources 
[BIOPAX](http://www.biopax.org/) files can be processed by PAX2GRAPHML.

For large files, the processing time can be long and a lot of RAM is needed.
To avoid this step, we make available updated preprocessed GRAPHML files containing reaction graphs.
 [GRAPHML](https://en.wikipedia.org/wiki/GraphML) is the manipulation format of PAX2GRAPHML.  [GRAPHML files](https://pax2graphml.genouest.org/db.html) provided in the GRAPHML data banks section are available online and processed on  the Genouest bioinformatics plateform thanks to [BIOMAJ](https://biomaj.genouest.org/). Using PAX2GRAPHML with GRAPHML is fast even with large graphs, thanks to the graph-tool library.

Thus, our approach, dedicated to regulation networks is based on two formats,  BIOPAX for knowledge management, standardization and interoperability of data banks and GRAPHML for simplicity and processing efficiency.

Other options includes  PAXTOOLS or SPARQL for queries. 

## Tutorials

A quick start example and more detailed tutorials are available:
 * [quick-start](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/first-step#quick-start)
 * [tutorials](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/first-step#tutorials)


## Installation
Installation is supported on Linux  MacOS and Windows. 


 * [Linux](#macos-linux-installation)
 * [MacOS](#macos-linux-installation)
 * [Windows](#windows-installation)
 * [Virtual-Machine](#installation-using-a-virtual-machine-windows-macos-linux)
 * [Docker](#docker-installation)

 


### Prerequisites


The package relies mainly on the python graph_tool module to extract, manipulate and save these graphs: [https://graph-tool.skewed.de/static/doc/quickstart.html](https://graph-tool.skewed.de/static/doc/quickstart.html).

Java is as well necessary for some sub-modules, like for BIOPAX export features,that use the reference library [PAXTOOLS](http://biopax.github.io/Paxtools/)


We suggest the docker or the conda  installation.


The docker container or the conda  packages provides the necessary environment to run the package (mainly, installation of python 3 and graph-tool).


### MacOS & Linux installation

#### Installation using conda

*  First install curl and conda

   1. install curl : [link](install_curl.md)
  ​

   1. install conda or miniconda : [link](https://docs.conda.io/projects/continuumio-conda/en/latest/user-guide/install/index.html)

*  option 1: install dependencies and package using conda:

  ```
  conda env remove -n p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/conda/environment-full.yml -o environment-full.yml 
  conda env create -f environment-full.yml
  conda activate p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
  python3 test.py
  ```


*  option 2: install dependencies using conda and pip:

  ```
  conda env remove -n p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/conda/environment-part.yml -o environment-part.yml 
  conda env remove -n p2g
  conda env create -f environment-part.yml  #install from anaconda.org
  conda activate p2g
  pip3 install pax2graphml #install from pypi
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
  python3 test.py
  ```


* run any script using pax2graphml after 'conda activate p2g' 

  ​example:
```
  conda activate p2g
  python3   -c "import pax2graphml  as p2g; print('package p2g installed')"

```


remarks:
1. 'pip3' and 'python3' commands may be replaced by 'pip' and 'python' on some OS. 
1. Please note that  the pax2graphml pypi package ( [https://pypi.org/project/pax2graphml/](https://pypi.org/project/pax2graphml/) )
does not declare all needed dependencies (graph-tool...). At least, prior graph-tool installation is needed. 
1. The pax2graphml conda package ( [https://anaconda.org/fjrmoreews/pax2graphml](https://anaconda.org/fjrmoreews/pax2graphml))  is a pure python package
Because of multi channel dependencies, the environment file is needed to fully install the package.


####  Install  pax2graphml from sources :
```
  #use conda for  complex  dependencies :
  conda env remove -n p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/conda/environment-part.yml -o environment-part.yml 
  conda env create -f environment-part.yml
  conda activate p2g
  #clone git repository 
  git clone https://gitlab.inria.fr/fmoreews/pax2graphml.git
  cd src 
  pip3  install -v . 
  pip3  show pax2graphml -f 
  
```
### Docker installation

#### ready to use Docker container

We provide a easy to use docker installation,tested with [Docker](https://www.docker.com/) on Linux and [Singularity](https://sylabs.io/)

The  PAX2GraphML docker container is available at [https://hub.docker.com/r/fjrmore/pax2graphml](https://hub.docker.com/r/fjrmore/pax2graphml)



[more details on the Pax2Graphml Docker page](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/docker/pax2graphml/README.md) 



### Linux installation (Ubuntu and Debian)

We describe here how to install pax2graphml with all dpendencies, graph-tool, jupyter lab and  java
please note that this installation can be tricky. 
we recommand the docker or conda  installations (see above). 

For a classical installation,  
see the 
[Ubuntu](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/ubuntu.md) 
page.




###  Windows installation


Installation on windows is possible using at least 2 options :


#### installation on Windows using docker-desktop
first install  docker-desktop : see https://docs.docker.com/docker-for-windows/install/  

when docker-desktop is installed, open a power-shell terminal, then entre the followig commands:


*  Pax2Graphml in python script

```
$vol= (pwd).Path

docker run -p 81:8888 -p 6006:6006 -it   -u user  -v ${vol}:/home/user  python3 -c "import pax2graphml; print('package p2g OK')"

curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test.py -o test.py
docker run -p 81:8888 -p 6006:6006 -it   -u user  -v ${vol}:/home/user  python3 test.py

```

```
#create your own script (mycript.py) , go in the directory where your script is located and execute it :.
cd script_dir
$vol= (pwd).Path
docker run -p 81:8888 -p 6006:6006 -it   -u user  -v ${vol}:/home/user  python3 mycript.py

```

How to run power-shell : see 
https://www.digitalcitizen.life/ways-launch-powershell-windows-admin/


 *  Pax2Graphml in jupyter lab
```
$vol= (pwd).Path
docker run -p 81:8888 -p 6006:6006 -it   -u user  -v ${vol}:/home/user   jupyter.sh
```

then launch a browser and go to the following url : http://localhost:81/lab

enter the password (password)

#### Installation on Windows using ubuntu terminal 

 See https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71?activetab=pivot:overviewtab

After you're environment configuration,
 follow the docker or conda + pip installation process (see above).


#### Installation using a Virtual Machine (Windows, MAcOS, Linux)
 A pre installed VM image is available (OVA file) and has been tested with Virtual-Box. 
See the  
[VM installation  page](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/vm.md) 






​    


### Install pax2graphml Virtual Machine

This instructions allow to use pax2graphml  on Windows, Linux and MacOs using a Virtual Machine (VM)

* first install [virtualbox](https://www.virtualbox.org)

* then download the pax2graphml OVA file (7Go) :

[https://pax2graphml.genouest.org/ova/pax2graphml_jupyter_ubuntu_20.04.ova](https://pax2graphml.genouest.org/ova/pax2graphml_jupyter_ubuntu_20.04.ova)
or [drive link](https://drive.google.com/file/d/1ccGdFjpExH8bpvpoG0IliryEBk05nOvf/view?usp=sharing)
or [alternate link](https://data-access.cesgo.org/index.php/s/WaZJg03u5ZQ1DzH)

* install the VM from the OVA file (double click on file and follow instructions)

* start the VM within virtualbox (better with internet connection)

* login with login *ubuntu*, password *ubuntu*


* the jupyter lab pax2graphml session is then automaticallly launched (url: http://localhost:80/lab? )

* enter the displayed password


* click and the example folder and test the existing python codes


remark: 
for keyboad issue (qwerty vs azerty ...) use the following command in a terminal within the VM session: 
```
setxkbmap fr  # azerty
#or 
setxkbmap en  # qwerty
```

### Linux installation (Ubuntu and Debian)
We describe here how to install pax2graphml with all dependencies, graph-tool, jupyter lab and  java
please note that this installation can be tricky. 

For the  docker or conda  installations (recommanded), see the [main page](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/docker/pax2graphml/README.md) 
 
 * For the classical installation (from sources), 

open a terminal and enter the following commands:



```
#Tested on Ubuntu 20.04 
echo "TUSER=$USER"
sudo -i
TUSER=myuser # put you user name here like displayed by the echo command
apt-get update
 
DISTRI=focal &&\
    DEBIAN_FRONTEND=noninteractive &&\
    TZ=America/New_York &&\
    WORKD=/home/$TUSER &&\
    PDIR=/home/$TUSER &&\
    PATH=$PATH:/home/$TUSER/.local/bin

apt-get remove -y gnupg &&    apt-get install -y gnupg2 dirmngr ca-certificates
 
echo "deb [ trusted=yes] https://downloads.skewed.de/apt $DISTRI main">>  /etc/apt/sources.list;
apt-get update --allow-unauthenticated
apt-get install -y  --no-install-recommends \
   mlocate sudo nano wget curl  xvfb \
   vim openjdk-8-jre 

apt-get install -y python3-pip python3-cairo \
 python3-gi-cairo tzdata libgtk3.0 libgtk-3-dev zip unzip  

apt-get install -y  python3-graph-tool

pip3 install --upgrade pip && pip3 install virtualenv
pip3 install pandas==1.2.3 && pip3 install matplotlib==3.3.4
pip3 install lxml==4.6.2 && pip3 install kmapper==1.2.0
pip3 install subprocrunner==1.2.1 && pip3 install pybiomart==0.2.0

pip3 install sphinx && pip3 install -U pip setuptools twine 

pip3 install jupyterlab==2.2.9
 
cd $WORKD

jupyter kernelspec list 



SRC=$WORKD/pax2graphml/src &&\ 
  USRBIN=/usr/bin/pax2graphml &&\
  LPAGE=/usr/local/lib/python3.8/dist-packages/notebook/templates 


mkdir -p $USRBIN && chmod -R 777 $USRBIN

 


git clone https://gitlab.inria.fr/fmoreews/pax2graphml.git
 
mkdir -p  /home/$TUSER/.jupyter/
cp ./pax2graphml/docker/pax2graphml/conf/*  /home/$TUSER/.jupyter/
sudo -i
cp -r $SRC/script $USRBIN/script && \
       cp -r $SRC/test $USRBIN/test 



sed -i 's/silent=False/silent=True/g' $SRC/pax2graphml/__init__.py &&\
    chmod -R 755 $USRBIN  && cd $SRC/ && bash install.sh && \
    python3   -c "import pax2graphml  as p2g; print('package p2g installed')"  

cp -r $WORKD/pax2graphml/docker/pax2graphml/entrypoint /usr/bin/entrypoint

chmod +x /usr/bin/entrypoint/* && chmod -R 777 $WORKD && rm -rf $SRC && \
  chown -R $TUSER $LPAGE && chmod 777 -R $LPAGE  \
 && mkdir -p /home/$TUSER/example   && chmod -R 777 /home/$TUSER/example


sed -i "s/home\/user/home\/$TUSER/g" /home/$TUSER/.jupyter/jupyter_notebook_config.py




exit
su $TUSER

 

```

 * Test using demo Jupyter + dataset
```
PATH=$PATH:/usr/bin/entrypoint/
SHOW_PWD=true
JUPYTER_PWD=1234
##################
demo.sh
 
##################
 
#or 
echo "downloading example dataset"
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
 
unzip data.zip && rm data.zip
mv data/* /home/$USER/example/

find /home/$USER/example/ -name "*.ipynb" -exec sed -i "s/home\/user/home\/$USER/g" '{}' \;

echo "running jupyter lab"
jupyter.sh



```

 * Test using scripting

```
python3   -c "import pax2graphml  as p2g; print('package p2g installed')"  

curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
python3 test.py

```








#### PAX2GraphML  in Docker

##### Docker Installation 

You  need to install docker first.
docker is available on linux and MacOs and Windows ( using Docker Desktop).


##### simple Jupyter launch with tutorial:

A tutorial is available with replayable scripts with demo data as a Jupyterlab instance in docker.
It can be run as follow:

```

docker run -p 80:8888 -p 6006:6006 -it \
   -e SHOW_PWD=true -e JUPYTER_PWD=password  \
   fjrmore/pax2graphml demo.sh


```
Wait the dataset is downloaded, 
then go to your browser at http://localhost (password=password)

##### simple Jupyter lab launch (void):


```

docker run -p 80:8888 -p 6006:6006 -it \
   -e SHOW_PWD=true -e JUPYTER_PWD=password  \
   fjrmore/pax2graphml jupyter.sh
 

```
##### Run python scripts from terminal:

```
#to obtain a shell for scripting :
docker run -it --rm  fjrmore/pax2graphml bash 
#then use the package in python script  ( import pax2graphml )
echo "import pax2graphml" >  myscript.py
python3 myscript.py

#edit file using nano or vi
nano myscript.py 
#download scripts and data using curl or wget
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
unzip data.zip
cd data/
ls *py

python3 t1_step_pax2graphml.py
python3 t2_1_step_properties.py

```
##### Jupyter lab notebook with custom data
 

 * run Pax2graphml Jupiter Lab and mount data using (-v / volume)
```
cd docker/pax2graphml # example data directory from git

IMAGENAME=fjrmore/pax2graphml

chmod -R 777 ./data

OPT=" -v $PWD/data:/home/user/example  -e SHOW_PWD=true -e JUPYTER_PWD=password "
docker run -p 80:8888 -p 6006:6006 -it $OPT -u user -w /home/user $IMAGENAME jupyter.sh

 
```
 you can also run  bash [runjupyter.sh](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/runjupyter.sh)


 * run Pax2graphml Jupiter Lab and copy data using docker cp 

 This option is mandatory when the volume mount is not possible (-v)
```
IMAGENAME=fjrmore/pax2graphml
OPT=" -e SHOW_PWD=true -e JUPYTER_PWD=password "
docker run --name p2g -p 80:8888 -p 6006:6006 -it $OPT -u user -w /home/user $IMAGENAME jupyter.sh
docker cp  data  p2g:/home/user/example
```
 
 * Jupyter With manual data upload


```
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
 
unzip data.zip && rm data.zip
mv data/* /home/$USER/example/

find /home/$USER/example/ -name "*.ipynb" -exec sed -i "s/home\/user/home\/$USER/g" '{}' \;
SHOW_PWD=true 
JUPYTER_PWD=password
jupyter.sh

```


#####  Docker build
To build the docker container, install docker and go to the docker folder: 

```
git clone https://gitlab.inria.fr/fmoreews/pax2graphml.git 
cd docker # repository docker folder
cd pax2graphmlenv 
IMAGENAMEENV=pax2graphmlenv
#build the container 
sudo docker build  -t $IMAGENAMEENV  ./
cd ..
cd pax2graphml
IMAGE=pax2graphml
bash build.sh
cd ..
#test the container

#execute pax2graphml scripts now
echo "import pax2graphml;print('p2g installed')" >  myscript.py
docker run  -it --rm -v $PWD:/export -w /export  $IMAGE python3 myscript.py
#or 
docker run  -it --rm -v $PWD:/export -w /export  $IMAGE bash
#then you are within the container
```








## PAX2Graphml first steps


### Quick start



A  quick start example is shown here.

More detailed tutorials can be found in [the tutorial dataset](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/docker/pax2graphml/data)


 * First [install Pax2graphml](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master#installation)


* Then download [an example BIOPAX file ](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data/biopax/G6P_neig_1.owl?inline=false) :

```
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data/biopax/G6P_neig_1.owl -o  G6P_neig_1.owl
```



* Download the following Python script [quick-start.py](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/first-step/quick-start.py?inline=false) :

```
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/first-step/quick-start.py -o  quick-start.py
```

and run the command 

```
python3 quick-start.py #or python quick-start.py
```


We provide as well a [Cytoscape cys file](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/first-step/vis_g6p.cys?inline=false) to visualize the manipulated network. It has been generated using the 'import /network/ graphml' Cytoscape option, from the generated Graphml file, G6P_neig_1.graphml.

### Tutorials


run the example scripts in command line or within Jupyter available here :
[tutorial dataset link](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip)
```
#download scripts and data using curl or wget
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
unzip data.zip
cd data/
ls *py
#Run  Pax2Graphml python scripts from terminal:
python3 t1_step_pax2graphml.py
python3 t2_1_step_properties.py

```






### Pax2Graphml Tutorials

The zipped version of the tutorial dataset can be found [here](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip?inline=false)

### How-To
 * First [install Pax2graphml](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master#installation)
 * Then use Jupyter lab or the Python command line

### Content

 * "ipynb" files are notebooks for Jupyter

 * "py" files can be run with python3 command line

 * The "result" folder will contain result files generated witin the tutorials

 * The "annot" folder contains files related to nodes and edges properties

 * The "biopax" folder contains example BIOPAX files

 * The "biopax_download" folder will store downloaded  BIOPAX files



[https://pax2graphml.genouest.org](https://pax2graphml.genouest.org/)

 The logging system 

### Logging and debugging Pax2Graphml 

The python logging system can be enabled or disabled (default) :

```
import pax2graphml as p2g
 
print("========default===========")
p2g.utils.resource_path()
print("========enable_logs===========")
p2g.enable_logs() # or p2g.verbose()
p2g.utils.resource_path()
print("======disable_logs=============")
p2g.disable_logs() 
p2g.utils.resource_path()

 
```


On some install, the python dependencies can produce unwanted logging messages :

This can be hidden by using teh following environment  variable:

```
P2G_IMPORT_SILENT=true
```


<br/> <br/><br/> document automatically generated from the markdown pages

