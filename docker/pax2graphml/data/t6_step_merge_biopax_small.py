#!/usr/bin/env python
# coding: utf-8

#  ** PAX2Graphml BIOPAX merge **
#  - Merge BIOPAX files using PAXTOOLS called from python
#  - Generate the reaction graph in GRAPHML
#  - Load the graph model

# In[1]:


import pax2graphml  as p2g
import timeit
import os.path

#rootPath="/home/user/example/"
rootPath=""
srcDir=rootPath+"biopax/"
resultPath=rootPath+"result/"

file1=srcDir+"G6P_neig_1.owl"
file2=srcDir+"ppara_custom.owl"
 


    


# In[2]:



biopaxfilelist=[file1,file2]
biopaxMergedFile= resultPath+"merged_small.owl"     
 

dowait=True
#warning large file, can be slow (near hakf an hour on a laptop)
if dowait==True:
  start = timeit.default_timer()
  response=p2g.pax_import.biopax_merge(biopaxfilelist,biopaxMergedFile)
  print(response)
  stop = timeit.default_timer()
  print("Time: %.2f s"%(  stop - start) ) 


# In[3]:



#warning large files,can be slow (many hours on a standard laptop )
graphmlfile=resultPath+"merged_small.graphml"

dowait=True
#warning large file, can be very slow
if dowait==True:
   start = timeit.default_timer()
   response=p2g.pax_import.biopax_to_reaction_graph(biopaxMergedFile,graphmlfile)
   print(response)
   stop = timeit.default_timer()
   print("Time: %.2f s"%(  stop - start) ) 


if os.path.isfile(graphmlfile  ):

  g=p2g.graph_explore.load_graphml(graphmlfile, directed=True)
  nodes=p2g.node_list(g)
  edges=p2g.edge_list(g)
  print("nodes count: %s edges count: %s "  %(len(nodes),len(edges)  )   )
else:
    print("file %s  not found.\nDid you try put dowait=True?" %(graphmlfile))


# In[ ]:




