#!/usr/bin/env python
# coding: utf-8

#  **PAX2Graphml Properties management example**
# 
#  - how to add custom node and edge properties from an external file,
#  - create property tabular files for nodes and edges
#  - how to use node and edge properties as mapping keys
#  - use Ensembl biomart API to generate new annotations and related node properties

# In[15]:


### create a simple graph with uniprot mapped to node,
## like in extracted graphs from Biopax (SPAIM,INFLUENCE)

import sys
import pax2graphml  as p2g
import graph_tool as gt
def graph_example():
  gg = gt.Graph() 
  gg.vp.uniprot=gg.new_vertex_property("string")
  gg.ep.group=gg.new_edge_property("string")


  v0=gg.add_vertex() 
  v1=gg.add_vertex() 
  v2=gg.add_vertex() 
  v3=gg.add_vertex() 
  v4=gg.add_vertex() 
  v5=gg.add_vertex()
  v6=gg.add_vertex()


  e1=gg.add_edge(v0,v5) 
  e2=gg.add_edge(v1,v5) 
  e3=gg.add_edge(v2,v5) 
  e4=gg.add_edge(v3,v5) 
  e5=gg.add_edge(v4,v5) 

  gg.vp["uniprot"][v0]="ACE2"
  gg.vp["uniprot"][v1]="ZP4"
  gg.vp["uniprot"][v3]="PPARA"
  gg.vp["uniprot"][v4]="P19367"
  gg.vp["uniprot"][v5]="JUND"
  gg.vp["uniprot"][v6]="Q96G03"






  gg.ep["group"][e1]="A"
  gg.ep["group"][e2]="A"
  gg.ep["group"][e3]="B"
  gg.ep["group"][e4]="B"
  gg.ep["group"][e5]="C"

  return gg

g=graph_example()

p2g.graph_explore.describe_graph(g)


# In[16]:


### add properties  for each node and edge of the graph using a property file 
## and the uniprot and group properties mapping
import pax2graphml  as p2g


            
propertiesNodefile="annot/annot_node.csv"
mapNodeKey="uniprot"
newNodeProperty="description"
p2g.properties.annot_node_from_file(g,propertiesNodefile,mapNodeKey,newNodeProperty)

propertiesEdgefile="annot/annot_edge.csv"
mapEdgeKey="group"
newEdgeProperty="confidence"
p2g.properties.annot_edge_from_file(g,propertiesEdgefile,mapEdgeKey,newEdgeProperty)


propertiesNodefile="annot/annot_node_index.csv"
mapNodeKey="index"
newNodeProperty="comment"
p2g.properties.annot_node_from_file(g,propertiesNodefile,mapNodeKey,newNodeProperty)

propertiesEdgefile="annot/annot_edge_index.csv"
mapEdgeKey="index"
newEdgeProperty="score"
p2g.properties.annot_edge_from_file(g,propertiesEdgefile,mapEdgeKey,newEdgeProperty)

print(p2g.graph_explore.describe_graph(g))
     
output_prop_node_file="annot/annot_node_out.csv"
output_prop_edge_file="annot/annot_edge_out.csv"

is_unique=p2g.properties.is_unique(g,"uniprot")
print("are each uniprot property values unique:%s" %(is_unique))

print("generating:%s" %(output_prop_node_file))
p2g.properties.annot_node_to_file(g,output_prop_node_file,"index","uniprot",'NA')

print("generating:%s" %(output_prop_edge_file))
p2g.properties.annot_edge_to_file(g,output_prop_edge_file,"index","confidence",'NA')



# In[17]:


### add Gene ontology term to each node of the graph using the included  biomart API
## and the uniprot mapping
 
    



 


      
g=graph_example()


protL=p2g.properties.property_values(g,"uniprot")

      
print("==find GO term using API======")
print("default API configuration (when conf=None)")
print(p2g.properties.__default_apî_conf())
      
species="rnorvegicus"  
#species="hsapiens"

conf={'dataset':species+'_gene_ensembl'}
print("current API configuration")
print(p2g.properties.__current_apî_conf(conf))
      
      
annot_map=p2g.properties.uniprot_to_go(protL,conf)
 

         
new_property="go"
primary_key="uniprot"
case_sensitive=False

p2g.properties.create_property_from_map(g,annot_map, primary_key,new_property,case_sensitive)
        
p2g.graph_explore.describe_graph(g)
        
 
"""
#test API
protL=['ZP4','ZP5','PPARA']

print("==find GO term using API======")

unimap=p2g.properties.uniprot_to_go(protL)


for k in unimap.keys():
   v=unimap[k]
   print("%s:%s" % (k,unimap[k]))

   
""" 


# In[18]:





print("==find HGNC symbol using API======")  
protL=['P19367', 'P35557','Q96G03']

conf={
       'server':'http://www.ensembl.org',
       'mart':'ENSEMBL_MART_ENSEMBL',
       'dataset':'hsapiens_gene_ensembl',
       'attr':[
           'ensembl_gene_id',
           'external_gene_name',
           'uniprot_gn_id',
           'hgnc_symbol'
          ],
       'searchkey' :'uniprot_gn_id',
       'uni_key' : 'UniProtKB Gene Name ID',    
       'annot_key' :'HGNC symbol'
}

g=graph_example()

annot_map=p2g.properties.ensembl_api(protL,conf)
print(annot_map)

new_property="hgnc_symbol"
primary_key="uniprot"
case_sensitive=False

p2g.properties.create_property_from_map(g,annot_map, primary_key,new_property,case_sensitive)
        
p2g.graph_explore.describe_graph(g)
        
    


# In[ ]:




