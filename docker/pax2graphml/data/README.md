### Pax2Graphml Tutorials

The zipped version of the tutorial dataset can be found [here](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip?inline=false)

### How-To
 * First [install Pax2graphml](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master#installation)
 * Then use Jupyter lab or the Python command line

### Content

 * "ipynb" files are notebooks for Jupyter

 * "py" files can be run with python3 command line

 * The "result" folder will contain result files generated witin the tutorials

 * The "annot" folder contains files related to nodes and edges properties

 * The "biopax" folder contains example BIOPAX files

 * The "biopax_download" folder will store downloaded  BIOPAX files



[https://pax2graphml.genouest.org](https://pax2graphml.genouest.org/)


A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A
A

