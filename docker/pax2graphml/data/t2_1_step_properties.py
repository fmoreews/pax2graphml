#!/usr/bin/env python
# coding: utf-8

# **PAX2Graphml Properties management example**
# 
#  - display all node and edge properties with types,
#  - create a property
#  - modify the type of properties
#  - manipulate properties values as list
#  - list all unique values of a property

# In[1]:


#
import os,sys
import numpy as np
import pax2graphml  as p2g
import graph_tool.all as gt


#rootPath="/home/user/example/"
rootPath=""

resultPath=rootPath+"result/"
graphmlfile=resultPath+"G6P_neig_1.graphml"
g=p2g.graph_explore.load_graphml(graphmlfile, directed=True)
print("#initial alias property")
print(p2g.properties.describe_properties(g,"alias"))

p2g.properties.string_to_list_property(g,"alias")
 
aliasList=p2g.properties.node_property_values(g,"alias")

print("#updated alias property")
print(p2g.properties.describe_properties(g,"alias"))
print("#all properties (node+edge)")
print(p2g.properties.describe_properties(g))

print("#all node property alias values:\n%s\n" %(aliasList))

spaimList=p2g.properties.edge_property_values(g,"spaim")
print("#all edge property spaim values:\n%s\n" %(spaimList))

#print(p2g.graph_explore.describe_graph(g))


# In[2]:




   
gg = gt.Graph() 
gg.vp.score=gg.new_vertex_property("string")
gg.ep.weight=gg.new_edge_property("string")

v0=gg.add_vertex() 
v1=gg.add_vertex() 

e1=gg.add_edge(v0,v1) 

gg.vp["score"][v0]="1"
gg.vp["score"][v1]="2"

gg.ep["weight"][e1]="10"
print(p2g.properties.describe_properties(gg,"score"))

p2g.properties.change_property_type(gg,"score", "int", "node") 
#p2g.properties.change_property_type(gg,"score", "double", "node") 
#p2g.properties.change_property_type(gg,"score", "bool", "node") 
#p2g.properties.change_property_type(gg,"weight", "string", "edge") 
p2g.properties.change_property_type(gg,"weight", "float", "edge") 

print(p2g.properties.describe_properties(gg,"score"))

print(p2g.graph_explore.describe_graph(gg))


# In[ ]:




