#!/usr/bin/env python
# coding: utf-8

#   **convert a reaction graph (substrat, product, activator, inibitor, modulator graph) to influence graph (causality signed graph)**
# 

# In[1]:



import pax2graphml as p2g
import logging 


#rootPath="/home/user/example/"
rootPath=""
srcDir=rootPath+"biopax/"
resultPath=rootPath+"result/"
    

biopaxfile1=rootPath+"influence/ephb2_biopax.owl" 
graphmlfile=rootPath+"influence/ephb2_biopax.graphml" 
graphmlfilechecked=rootPath+"influence/ephb2_biopax_checked.graphml" 


outputGraphInfluence=rootPath+"influence/influence.graphml"
outputDisplaySpaim=rootPath+"influence/spaim.png"
outputDisplaySpaimChecked=rootPath+"influence/spaim_checked.png"
outputDisplayInfluence=rootPath+"influence/influence.png"

p2g.pax_import.biopax_to_reaction_graph(biopaxfile1,graphmlfile)
g=p2g.graph_explore.load_graphml(graphmlfile, directed=True)

 


conf={"nodelabel":"name","edgelabel":"spaim"}     
#conf=None
p2g.graph_explore.save_image(g,outputDisplaySpaim, 3000,conf)    
#p2g.utils.save_image(g,outputDisplay)
print('start')
p2g.pax_import.prepare_spaim(graphmlfile,graphmlfilechecked,None)
g2=p2g.graph_explore.load_graphml(graphmlfilechecked, directed=True)
p2g.graph_explore.save_image(g2,outputDisplaySpaimChecked, 3000,conf)    

p2g.pax_import.influence_graph(graphmlfilechecked,outputGraphInfluence,outputDisplayInfluence)
print('end')


# In[3]:


biopaxfile=rootPath+"influence/G6P_neig_1.owl"
graphmlfile=rootPath+"influence/G6P_neig_1.graphml"
graphmlChecked=rootPath+"influence/G6P_neig_1_checked.graphml"
imageUnChecked=rootPath+"influence/G6P_neig_1.png"
imageChecked=rootPath+"influence/G6P_neig_1_checked.png"
print('biopax2spaimGraphMl')
p2g.pax_import.biopax_to_reaction_graph(biopaxfile,graphmlfile)
print('prepare_spaim')


logger = logging.getLogger()
logger.disabled = False
logging.basicConfig(level="info")

p2g.pax_import.prepare_spaim(graphmlfile,graphmlChecked,imageChecked)


  
outputGraphInfluence=rootPath+"influence/G6P_neig_1_influence.graphml"
outputDisplayInfluence=rootPath+"influence/G6P_neig_1_influence.png"
minNode=3
maxNode=1000

conf={"nodelabel":"name","edgelabel":"spaim"}     
#conf=None

g=p2g.graph_explore.load_graphml(graphmlfile, directed=True)
p2g.graph_explore.save_image(g,imageUnChecked, 30000,conf)    

g=p2g.graph_explore.load_graphml(graphmlChecked, directed=True)
p2g.graph_explore.save_image(g,imageChecked, 30000,conf)    
print("generating influence_graph in %s" %(outputGraphInfluence))
p2g.pax_import.influence_graph(graphmlChecked,outputGraphInfluence,outputDisplayInfluence)



# In[ ]:




