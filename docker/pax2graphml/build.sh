#!/bin/bash
#mv ./data/* ../data/

IMAGENAME=pax2graphml

TMPDIR="./tmp"

SRCDIR="../../src"

if [ -d $TMPDIR ]; then 
  rm -rf $TMPDIR 
fi
mkdir $TMPDIR 
 
cp -r $SRCDIR $TMPDIR/ && chmod -R 777 $TMPDIR

mkdir -p ./data && chmod -R 777 ./data

docker build  -t $IMAGENAME  ./

#mv ../data/* ./data/

if [ -d $TMPDIR ]; then 
  rm -rf $TMPDIR 
fi
