import json
import os
from notebook.auth import passwd;
jsonfile="/home/user/.jupyter/jupyter_notebook_config.json"
if "JUPYTER_PWD" in os.environ:
   jp=os.environ["JUPYTER_PWD"]

   key=passwd(jp)
   d={"NotebookApp": {"password": key}}
   #print(d)
   with open(jsonfile, 'w') as outfile:
      json.dump(d, outfile)
   print("jupyter password  changed")
else:
    print("jupyter password not changed")

