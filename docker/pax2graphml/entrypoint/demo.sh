#!/bin/bash
CDIR=$PWD
echo "downloading example dataset"
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
 
unzip data.zip && rm data.zip
mv data/* /home/$USER/example/

find /home/$USER/example/ -name "*.ipynb" -exec sed -i "s/home\/user/home\/$USER/g" '{}' \;

echo "running jupyter lab"
jupyter.sh
