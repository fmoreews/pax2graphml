#!/bin/bash

BUF=`cat login.tmpl`


TARGET=$LPAGE/login.html

if [ ! -z $JUPYTER_PWD ]; then

  MSG=""

  if [ ! -z $SHOW_PWD ]; then
    
    if [ "$SHOW_PWD" == "true" ]; then


     MSG="the login is : $JUPYTER_PWD"
     echo "========================"
    fi

  fi

  BUF2=$(echo $BUF| sed "s/<!--MSG-->/$MSG/")
 
  echo " $BUF2 " | cat > /tmp/login.html
  rm -f $TARGET
  mv /tmp/login.html $TARGET
  chmod 644 $TARGET
 
fi
