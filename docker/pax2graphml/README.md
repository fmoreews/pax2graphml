#### PAX2GRAPHML  in Docker

##### Docker Installation 

You  need to [install docker](https://docs.docker.com/get-docker/) first.
Docker is available on linux and MacOs and Windows ( using Docker Desktop).

 
##### simple Jupyter launch with pre-loaded tutorials:

A tutorial is available with replayable scripts with demo data as a Jupyterlab instance in docker.
It can be run as follow:


```

docker run -p 80:8888 -p 6006:6006 -it \
   -e SHOW_PWD=true -e JUPYTER_PWD=password  \
   fjrmore/pax2graphml demo.sh

#please not that with 'demo.sh'  the dataset will be downloaded each time
#redefine the password as needed using :JUPYTER_PWD=your-password-here
#hide the password with  SHOW_PWD=false
```
Wait the dataset is downloaded, 
then go to your browser at http://localhost (password=password)



##### simple Jupyter lab launch (void):


```

docker run -p 80:8888 -p 6006:6006 -it \
   -e SHOW_PWD=true -e JUPYTER_PWD=password  \
   fjrmore/pax2graphml jupyter.sh
 

```
##### Run python scripts from terminal:

```
#to obtain a shell for scripting :
docker run -it --rm  fjrmore/pax2graphml bash 
#then use the package in python script  ( import pax2graphml )
echo "import pax2graphml" >  myscript.py
python3 myscript.py

#edit file using nano or vi
nano myscript.py 
#download scripts and data using curl or wget
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
unzip data.zip
cd data/
ls *py

python3 t1_step_pax2graphml.py
python3 t2_1_step_properties.py

```


Remark: 
Please not that the docker option " -u root " allows administrator
 privileges for installation within the containers. 

##### Jupyter lab notebook with custom data
 

 * run PAX2GRAPHML Jupiter Lab and mount data using (-v / volume)
```
cd docker/pax2graphml # example data directory from git

IMAGENAME=fjrmore/pax2graphml

chmod -R 777 ./data

OPT=" -v $PWD/data:/home/user/example  -e SHOW_PWD=true -e JUPYTER_PWD=password "
docker run -p 80:8888 -p 6006:6006 -it $OPT -u user -w /home/user $IMAGENAME jupyter.sh

 
```
 you can also run  bash [runjupyter.sh](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/runjupyter.sh)


 * run PAX2GRAPHML Jupiter Lab and copy data using docker cp 

 This option is mandatory when the volume mount is not possible (-v)
```
IMAGENAME=fjrmore/pax2graphml
OPT=" -e SHOW_PWD=true -e JUPYTER_PWD=password "
docker run --name p2g -p 80:8888 -p 6006:6006 -it $OPT -u user -w /home/user $IMAGENAME jupyter.sh
docker cp  data  p2g:/home/user/example
```
 
Please note that 2 environment variables are created (SHOW_PWD and JUPYTER_PWD).
SHOW_PWD allows to display the password in the jupyter start page.
JUPYTER_PWD redefines the password.

 * Jupyter With manual data upload


```
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
 
unzip data.zip && rm data.zip
mv data/* /home/$USER/example/

find /home/$USER/example/ -name "*.ipynb" -exec sed -i "s/home\/user/home\/$USER/g" '{}' \;
SHOW_PWD=true 
JUPYTER_PWD=password
jupyter.sh

```


#####  Docker build
To build the Docker container, install Docker and go to the docker folder: 

```
git clone https://gitlab.inria.fr/fmoreews/pax2graphml.git 
cd docker # repository docker folder
cd pax2graphmlenv 
IMAGENAMEENV=pax2graphmlenv
#build the container 
sudo docker build  -t $IMAGENAMEENV  ./
cd ..
cd pax2graphml
IMAGE=pax2graphml
bash build.sh
cd ..
#test the container

#execute PAX2GRAPHML scripts now
echo "import pax2graphml;print('p2g installed')" >  myscript.py
docker run  -it --rm -v $PWD:/export -w /export  $IMAGE python3 myscript.py
#or 
docker run  -it --rm -v $PWD:/export -w /export  $IMAGE bash
#then you are within the container
```


##### Other container solutions & HPC
 

As a  Docker alternative on desktop computer  or for an HPC  cluster deployment,
 you can  use [Singularity](https://sylabs.io/singularity/).

[We use Singularity](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/biomaj) to process large BIOPAX files with PAX2GRAPHML,  on our cluster and generate our [GraphML data banks](https://pax2graphml.genouest.org/db.html). 



#






