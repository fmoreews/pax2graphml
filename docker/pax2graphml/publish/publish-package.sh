#!/bin/bash

set -e

echo "start publishing to $1"

echo "python3 setup.py sdist"
python3 setup.py sdist

echo "twine check    dist/*"
twine check    dist/*

echo "twine upload  --non-interactive --repository $1 --config-file /publish/.pypirc  dist/*"
twine upload  --non-interactive --repository $1 --config-file /publish/.pypirc  dist/*

echo "end publishing to $1"
