#!/bin/bash

set -e

IMAGENAME=pax2graphml
 

TMPDIR=$(realpath "./tmp")

SRCDIR="../../src"

#PPREPO=pypitest
PPREPO=pypi



if [ -d $TMPDIR ]; then 
  rm -rf $TMPDIR 
fi
mkdir $TMPDIR 
 
cp -r $SRCDIR $TMPDIR/ && chmod -R 777 $TMPDIR



#docker run  -it -v $PWD/publish:/publish -v $TMPDIR:/src  --rm $IMAGENAME bash -c "cd /publish && bash publish-package.sh $PPREPO" 

docker run  -it -v $PWD/publish:/publish -v $TMPDIR/:/tmpsrc -u root --rm $IMAGENAME bash -c "mv /tmpsrc/src /src && cd /src && ls -l &&  bash /publish/publish-package.sh $PPREPO" 

if [ -d $TMPDIR ]; then 
  rm -rf $TMPDIR 
fi
