from setuptools import setup
import versioneer

requirements = [
     'matplotlib','numpy','pandas','lxml',
]

setup(
    name='pax2graphml',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="Large-scale Regulation Network in Python using BIOPAX and Graphml",
    license="MIT",
    author="FJR Moreews",
    author_email='fjrmoreews@gmail.com',
    url='https://github.com/fjrmoreews/pax2graphml',
    packages=['pax2graphml'],
    entry_points={
        'console_scripts': [
            'pax2graphml=pax2graphml.cli:cli'
        ]
    },
    install_requires=requirements,
    keywords='pax2graphml',
    classifiers=[
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.9',
    ]
)
