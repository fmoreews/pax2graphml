from argparse import ArgumentParser
from pax2graphml import __version__

def cli(args=None):
    p = ArgumentParser(
        description="Large-scale Regulation Network in Python using BIOPAX and Graphml",
        conflict_handler='resolve'
    )
    p.add_argument(
        '-V', '--version',
        action='version',
        help='Show the pax2graphml version number and exit.',
        version="pax2graphml %s" % __version__,
    )

    args = p.parse_args(args)


    print("CLI template")

    


if __name__ == '__main__':
    import sys
    cli(sys.argv[1:])
