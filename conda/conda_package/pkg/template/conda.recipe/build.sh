echo "================="
#echo $PWD
#echo "================="
#CDIR=$pwd

cd $SRC_DIR
ls -slh
echo "=======subprocrunner=========="
if [ -d "$subprocrunner" ]; then
  echo "subprocrunner dir exists"
else
  git clone https://github.com/thombashi/subprocrunner.git
fi
cd subprocrunner
python -m pip install   --ignore-installed -vv . 
echo "=======pybiomart=========="
cd $CDIR

if [ -d "pybiomart" ]; then
  echo "pybiomart dir exists"
else
  git clone https://github.com/jrderuiter/pybiomart.git
fi
cd pybiomart
python -m pip install   --ignore-installed -vv . 
echo "=======pax2graphml=========="
#cd $CDIR
#
cd $SRC_DIR
#cd pax2graphml
ls -slh
python -m pip install   --ignore-installed -vv . 
pip show pax2graphml -f
 
