===============================
pax2graphml
===============================


.. image:: https://img.shields.io/travis/fjrmoreews/pax2graphml.svg
        :target: https://travis-ci.org/fjrmoreews/pax2graphml
.. image:: https://circleci.com/gh/fjrmoreews/pax2graphml.svg?style=svg
    :target: https://circleci.com/gh/fjrmoreews/pax2graphml
.. image:: https://codecov.io/gh/fjrmoreews/pax2graphml/branch/master/graph/badge.svg
   :target: https://codecov.io/gh/fjrmoreews/pax2graphml


Large-scale Regulation Network in Python using BIOPAX and Graphml
