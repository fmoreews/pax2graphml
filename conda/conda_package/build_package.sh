#!/bin/bash
echo "preparing conda package"
RDIR=$PWD/pkg
SRCDIR=$PWD/../../src/

BUILDDIR=$RDIR/build
TDIR=$RDIR/template
#cd $RDIR
rm -rf $BUILDDIR
mkdir -p $BUILDDIR
cp -r $TDIR/* $BUILDDIR/
cp -r $SRCDIR/pax2graphml/* $BUILDDIR/pax2graphml/
 
echo "following step:"
#conda install conda-build
echo conda build   $BUILDDIR   
#anaconda login
#anaconda upload $ARCH_FILE
conda build purge
