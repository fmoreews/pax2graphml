
#  **PAX2Graphml first steps tutorial**
#  
#   - Convert a BIOPAX file to a GRAPHML file containing a reaction graph
#   - Efficiently iterate on the graph nodes and edges
#   - Display the GRAPHML file  content
#   - Import and export node properties
#   - Extract unique node property values 
#   - Compute graph connected components
#   - Extract  subgraphs by datasources 
#   - Merge nodes based on properties and preserve edges
#   - Merge 2 graphs based on node property values



import pax2graphml  as p2g
import os




rootPath=""
resultPath=""

biopaxfile1=rootPath+"G6P_neig_1.owl"
graphmlfile=resultPath+"G6P_neig_1.graphml"




p2g.pax_import.biopax_to_reaction_graph(biopaxfile1,graphmlfile)




g=p2g.graph_explore.load_graphml(graphmlfile, directed=True)
nodes=p2g.node_list(g)
edges=p2g.edge_list(g)
print("nodes count: %s edges count: %s "  %(len(nodes),len(edges)  )   )





for n in nodes[:3]:
    print("  node %s"  %(n)  )    





for e in edges[:3]:
    print("  edge %s"  %(e)  )   





xmlnodes=p2g.graph_explore.graphml_xml_string(graphmlfile ,[3,20,21,200],"node") 
for x in xmlnodes:
    print("%s\n\n" %(x))





propertiesfile=resultPath+"properties.json"
p2g.pax_import.name_alias(biopaxfile1,propertiesfile)






p2g.pax_import.join_annotation(g,propertiesfile,"label","importedfield","any")
graphmlOutFile=resultPath+"out.graphml"
p2g.graph_explore.save_graphml(g,graphmlOutFile,True)





"""
Extraction of all values of a node property  
"""

providerList=p2g.properties.property_values(g,"provider")
print("all datasources:%s\n" %(providerList))

uniprotList=p2g.properties.property_values(g,"uniprot")
print("all uniprot:%s\n" %(uniprotList))
      
chebiList=p2g.properties.property_values(g,"chebi")
print("all chebi:%s\n" %(chebiList))
      





"""
Extraction of Connected components 
(entity neighbourhood)
example of the neighbourhood of a list of entities (protein +metabolite)
"""

#    print(mccid)
targ=["P46976","P11217","Q96G03","P13807","TPNH","PGYM b dimer:AMP","ADP"]    
#targ=["ADP"]    
sg,targmap=p2g.extract.connected_component_by_annotation(g,targ,["uniprot","name"])    
print('------sub-graph-including-inputs------')
print(p2g.graph_explore.summary(sg))

subsetgraphmlOutFile=resultPath+"subset.graphml"
print("--------exporting sub graph to file: %s-------" %(subsetgraphmlOutFile) )
p2g.graph_explore.save_graphml(sg,subsetgraphmlOutFile,True)
print('---------------')






"""
Extraction of subgraphs by datasources
"""


print(p2g.graph_explore.summary(g))
# add update a prop value list to a value
print("we extract sub graphs by provider(datasource) values\n") 
subgraphList=p2g.extract.subgraphs_by_datasource(g)
 
for d in subgraphList:
    pvlist=d["datasource"]
    ssg=d["subgraph"]
    print("  datasources:%s" %(pvlist))
    print(p2g.graph_explore.summary(ssg))
    
print("we define a default value ('any') for missing value ")

p2g.properties.defaultNodeValue(g,"provider","any")

print("and  extract the sub graphs\n") 
subgraphList2=p2g.extract.subgraphs_by_datasource(g,False)
for d in subgraphList2:
    pvlist=d["datasource"]
    ssg=d["subgraph"]
    print("  datasources:%s" %(pvlist))
    print(p2g.graph_explore.summary(ssg))
    
    





"""
 how to merge nodes and keep all edges
"""

import graph_tool as gt
 
    
gg = gt.Graph() 
gg.vp.group=gg.new_vertex_property("int")
gg.vp.tag=gg.new_vertex_property("string")
gg.ep.interact=gg.new_edge_property("string")

v0=gg.add_vertex() 
v1=gg.add_vertex() 
v2=gg.add_vertex() 
v3=gg.add_vertex() 
v4=gg.add_vertex() 
v5=gg.add_vertex()

gg.add_edge(v0,v5) 
gg.add_edge(v1,v5) 
gg.add_edge(v2,v5) 
gg.add_edge(v3,v5) 
gg.add_edge(v4,v5) 

for v in gg.vertices(): 
   gg.vp["group"][v]=1
   gg.vp["tag"][v]="keep"
    
nnode=gg.add_vertex() 
gg.add_edge(v0,nnode) 
gg.add_edge(v1,nnode) 
gg.add_edge(nnode,nnode) 

gg.vp["group"][nnode]=2
gg.vp["tag"][nnode]="new"

p2g.properties.default_edge_value(gg,"interact","+")



p2g.graph_explore.describe_graph(gg)     
print(p2g.graph_explore.summary(gg))
print("we merge node %s with %s " %(v5,nnode))
p2g.extract.merge_nodes(gg,v5,nnode)

p2g.graph_explore.describe_graph(gg)     
print(p2g.graph_explore.summary(gg))
 





"""
   how to merge 2 graphs based on node property values
"""

import graph_tool as gt

 
    
def defineGraph1():
    
  gr = gt.Graph() 
  gr.vp.tag=gr.new_vertex_property("string")
  gr.vp.uri=gr.new_vertex_property("string")
  gr.vp.alias=gr.new_vertex_property("string")
  gr.vp.biopaxType=gr.new_vertex_property("string")    
  
  v0=gr.add_vertex() 
  v1=gr.add_vertex() 
  v2=gr.add_vertex() 
  v3=gr.add_vertex() 
  v4=gr.add_vertex() 
  v5=gr.add_vertex()

  gr.add_edge(v0,v2) 
  gr.add_edge(v1,v3) 
  gr.add_edge(v2,v4) 
  gr.add_edge(v3,v2) 
  gr.add_edge(v4,v5) 
  

  for v in gr.vertices(): 
      gr.vp["tag"][v]="graph1"

  gr.vp["uri"][v1]="http://www.reactome.org/biopax/56/71387#SmallMolecule28"
  gr.vp["alias"][v1]="Adenosine 5'-diphosphate;ADP"
  gr.vp["biopaxType"][v1]="SmallMolecule"
  

  gr.vp["uri"][v2]="http://www.reactome.org/biopax/56/71387#Protein130"
  gr.vp["alias"][v2]="phosphorylated glycogen synthase 1;phospho-GYS1;p-S-GYS1"
  gr.vp["biopaxType"][v2]="Protein"


  gr.vp["uri"][v3]="http://www.reactome.org/biopax/56/71387#Protein5540"
  gr.vp["alias"][v3]="p-S-GYS1X;pS_GYS1X"
  gr.vp["biopaxType"][v3]="Protein"

  return gr


 
def defineGraph2():
    
  gr = gt.Graph() 
  gr.vp.tag=gr.new_vertex_property("string")
  gr.vp.uri=gr.new_vertex_property("string")
  gr.vp.alias=gr.new_vertex_property("string")
  gr.vp.biopaxType=gr.new_vertex_property("string")    
 
  v0=gr.add_vertex() 
  v1=gr.add_vertex() 
  v2=gr.add_vertex() 
  v3=gr.add_vertex() 
  v4=gr.add_vertex() 
  v5=gr.add_vertex()

  gr.add_edge(v1,v0) 
  gr.add_edge(v1,v2) 
  gr.add_edge(v4,v3) 
  gr.add_edge(v2,v4) 
  gr.add_edge(v0,v2) 
  gr.add_edge(v1,v5) 
  

  for v in gr.vertices(): 
      gr.vp["tag"][v]="graph2"

  gr.vp["uri"][v1]="http://www.reactome.org/biopax/56/71387#SmallMolecule28"
  gr.vp["alias"][v1]="Adenosine 5'-diphosphate;ADP"
  gr.vp["biopaxType"][v1]="SmallMolecule"
  

  gr.vp["uri"][v2]="http://www.reactome.org/biopax/56/71387#Protein130"
  gr.vp["alias"][v2]="p-S-GYS1"
  gr.vp["biopaxType"][v2]="Protein"


  gr.vp["uri"][v3]="http://www.reactome.org/biopax/56/71387#Protein129"
  gr.vp["alias"][v3]="oligo((1,4)-alpha-glucosyl) GN-1;oligo((1,4)-alpha-glucosyl) GYG1"
  gr.vp["biopaxType"][v3]="Protein"

  gr.vp["uri"][v4]="http://www.reactome.org/biopax/56/71387#Protein75175"
  gr.vp["alias"][v4]="pS_GYS1X"
  gr.vp["biopaxType"][v4]="Protein"

  return gr



  #for v in g1.vertices(): 
 
 

g1=defineGraph1()
g2=defineGraph2()
#properties=["uri"]
properties=["alias"]
##todo :on alias (list)
g3=p2g.extract.merge_graph(g1,g2,properties)
print("-------------")
print("first graph\n")
p2g.graph_explore.describe_graph(g1)   
print("-------------")      
print("-------------")
print("second graph\n")
p2g.graph_explore.describe_graph(g2)   
print("-------------")      
print("-------------")
print("merged graph\n")
print("-------------")
p2g.graph_explore.describe_graph(g3)     


 






gr3=p2g.extract.merge_graph(g1,g2,properties)
gr3b=gr3.copy()
gr4=p2g.extract.merge_node_by_property(gr3b,["tag"])
print("-------------")
print("first graph\n")
p2g.graph_explore.describe_graph(gr3)   
print("resulting graph\n")
p2g.graph_explore.describe_graph(gr4)   
print("-------------")         




