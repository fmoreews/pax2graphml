## PAX2Graphml first steps


### Quick start



A  quick start example is shown here.

More detailed tutorials can be found in [the tutorial dataset](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/docker/pax2graphml/data)


 * First [install Pax2graphml](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master#installation)


* Then download [an example BIOPAX file ](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data/biopax/G6P_neig_1.owl?inline=false) :

```
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data/biopax/G6P_neig_1.owl -o  G6P_neig_1.owl
```



* Download the following Python script [quick-start.py](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/first-step/quick-start.py?inline=false) :

```
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/first-step/quick-start.py -o  quick-start.py
```

and run the command 

```
python3 quick-start.py #or python quick-start.py
```

We provide as well a [Cytoscape cys file](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/first-step/vis_g6p.cys?inline=false) to visualize the manipulated network. It has been generated using the 'import /network/ graphml' Cytoscape option, from the generated Graphml file, G6P_neig_1.graphml. 

The file spaim_style.xml contains a cystoscape visual style  to help to clearly display a graphml file generated with Pax2graphml.

The generated  .graphml file can be displayed in the [yEd](https://www.yworks.com/products/yed) editor too . Use the  graph_explore.save_yed_graphml function for this purpose for better rendering.

  

To understand the BIOPAX underlying structure,  we propose to use  two softwares:

- ChiBE  for  interpreted BIOPAX vizualization :    https://github.com/PathwayCommons/chibe
- NEO4J for a raw BIOPAX vizualization solution : the neo4j folder hold a ready made docker installation  for RDF/XML import 

###  Tutorials


run the example scripts in command line or within Jupyter available here :
[tutorial dataset link](https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip)
```
#download scripts and data using curl or wget
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
unzip data.zip
cd data/
ls *py
#Run  Pax2Graphml python scripts from terminal:
python3 t1_step_pax2graphml.py
python3 t2_1_step_properties.py

```





