# BIOPAX visualization with NEO4J

BIOPAX can be visualize with NEO4j, as any RDF/XML. 

## Installation

 RDF/XML import

1. The docker image is configurated with  neo4J and the neosemantics plugin 

   using docker:

   ```
   bash run-neo4j-docker.sh
   ```
   or 
  ```
  docker run -it  -p "7474:7474" -p "7687:7687" 
      -v $PWD/plugins:/plugins -v $PWD/conf:/tmp/conf/  \
      -v $PWD/export:/export  -v $PWD/import:/var/lib/neo4j/import \
      -v $PWD/entrypoint.sh:/e.sh -e  NEO4JAUTH=neo4j/admin \
      neo4j:3.5.3 bash -c " bash /e.sh "

  ```
 ## Usage
  
   Go to http://localhost:7474/browser/

   The login is : neo4j/admin
 
