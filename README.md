# PAX2GRAPHML 

## Features

The package PAX2GRAPHML (p2g)  allows to manipulate   BioPAX data sources transformed as GRAPHML files.

It is expecially design  around the SPAIM model (Substrate, Product, Activator, Inhibitor, Modulator). 
In this model, reactions and biological entities are represented as nodes. The edges hold the roles of the entities (is_substrat_of, is_activator_of...)
Among other things, it allows to visualize and analyze the graphs, to extract sub-components and to build an influence graph.

Input graphs are in .graphml format, assumed under the SPAIM model . 
## Documentation 
The Python code documentation is available on [https://fjrmoreews.github.io/pax2graphml/](https://fjrmoreews.github.io/pax2graphml/)
<br/>
This current introduction in available in one file as pdf : [p2g-getting-started.pdf](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/p2g-getting-started.pdf)
## Data Sources 
[BIOPAX](http://www.biopax.org/) files can be processed by PAX2GRAPHML.

For large files, the processing time can be long and a lot of RAM is needed.
To avoid this step, we make available updated preprocessed GRAPHML files containing reaction graphs.
 [GRAPHML](https://en.wikipedia.org/wiki/GraphML) is the manipulation format of PAX2GRAPHML.  [GRAPHML files](https://pax2graphml.genouest.org/db.html) provided in the GRAPHML data banks section are available online and processed on  the Genouest bioinformatics plateform thanks to [BIOMAJ](https://biomaj.genouest.org/). Using PAX2GRAPHML with GRAPHML is fast even with large graphs, thanks to the graph-tool library.

Thus, our approach, dedicated to regulation networks is based on two formats,  BIOPAX for knowledge management, standardization and interoperability of data banks and GRAPHML for simplicity and processing efficiency.

Other options includes  PAXTOOLS or SPARQL for queries. 

## Tutorials

A quick start example and more detailed tutorials are available :

 * [quick-start](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/first-step#quick-start)
 * [tutorials](https://gitlab.inria.fr/fmoreews/pax2graphml/-/tree/master/first-step#tutorials)


## Installation
Installation is supported on Linux  MacOS and Windows. 


 * [Linux](#macos-linux-installation)
 * [MacOS](#macos-linux-installation)
 * [Windows](#windows-installation)
 * [Virtual-Machine](#installation-using-a-virtual-machine-windows-macos-linux)
 * [Docker](#docker-installation)

 


### Prerequisites


The package relies mainly on the Python graph-tool module to extract, manipulate and save these graphs: [https://graph-tool.skewed.de/static/doc/quickstart.html](https://graph-tool.skewed.de/static/doc/quickstart.html).

Java is as well necessary for some sub-modules, like for BIOPAX export features, that use the reference library [PAXTOOLS](http://biopax.github.io/Paxtools/)


We suggest the Docker or the Conda installation. For simplicity, you can use the pre-installed VM.


The Docker container or the Conda  packages provides the necessary environment to run the package (mainly, installation of python 3 and graph-tool).


### MacOS & Linux installation

#### Installation using Conda

*  First install curl and Conda

   1. install curl : [link](install_curl.md)
  ​

   1. install Conda or Miniconda : [link](https://docs.conda.io/projects/continuumio-conda/en/latest/user-guide/install/index.html)

*  option 1: install dependencies and package using Conda:

  ```
  conda env remove -n p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/conda/environment-full.yml -o environment-full.yml 
  conda env create -f environment-full.yml
  conda activate p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
  python3 test.py
  ```


*  option 2: install dependencies using Conda and pip:

  ```
  conda env remove -n p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/conda/environment-part.yml -o environment-part.yml 
  conda env remove -n p2g
  conda env create -f environment-part.yml  #install from anaconda.org
  conda activate p2g
  pip3 install pax2graphml #install from pypi
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
  python3 test.py
  ```


* run any script using PAX2GRAPHML after 'conda activate p2g' 

  ​example:
```
  conda activate p2g
  python3   -c "import pax2graphml  as p2g; print('package p2g installed')"

```


remarks:
1. 'pip3' and 'python3' commands may be replaced by 'pip' and 'python' on some OS. 
1. Please note that  the PAX2GRAPHML pypi package ( [https://pypi.org/project/pax2graphml/](https://pypi.org/project/pax2graphml/) )
does not declare all needed dependencies (graph-tool...). At least, prior graph-tool installation is needed (see installation sections above). 
1. The PAX2GRAPHML Conda package ( [https://anaconda.org/fjrmoreews/pax2graphml](https://anaconda.org/fjrmoreews/pax2graphml))  is only the core Python package. Because of multi conda channel dependencies, the environment file is needed to fully install PAX2GRAPHML (see installation sections above).


Thes procedures has been tested on MacOS (MacOS Mojave - 10.14.6) and  Linux (Ubuntu 20.04 and 16.04).

####  Install  PAX2GRAPHML from sources :

This installation option targets p2g developpers and contributors.

```
  #use conda for  complex  dependencies :
  conda env remove -n p2g
  curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/conda/environment-part.yml -o environment-part.yml 
  conda env create -f environment-part.yml
  conda activate p2g
  #clone git repository 
  git clone https://gitlab.inria.fr/fmoreews/pax2graphml.git
  cd src 
  pip3  install -v . 
  pip3  show pax2graphml -f 
  
```
### Docker installation

#### ready to use Docker container

We provide a easy to use Docker installation,tested with [Docker](https://www.docker.com/) on Linux and [Singularity](https://sylabs.io/)

The  PAX2GRAPHML Docker container is available at [https://hub.docker.com/r/fjrmore/pax2graphml](https://hub.docker.com/r/fjrmore/pax2graphml)



[more details on the Pax2Graphml Docker page](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/docker/pax2graphml/README.md) 



### Linux installation (Ubuntu and Debian)

We describe here how to install PAX2GRAPHML with all dependencies, graph-tool, jupyter lab and  java
please note that this installation can be tricky. 
we recommand the Docker or Conda  installations (see above). 

For a classical installation,  
see the 
[Ubuntu](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/ubuntu.md) 
page.




###  Windows installation


Installation on windows is possible using at least 2 options :


#### Installation on Windows using Docker-desktop
This procedure has been tested on Windows 10 Enterprise.
first install  Docker-desktop : see https://docs.docker.com/docker-for-windows/install/  

when Docker-desktop is installed, open a power-shell terminal, then entre the followig commands:


*  PAX2GRAPHML in Python scripts

```
$vol= (pwd).Path

docker run -p 81:8888 -p 6006:6006 -it  -v ${vol}:/home/user fjrmore/pax2graphml  python3 -c "import pax2graphml; print('package p2g OK')"

curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
docker run -p 81:8888 -p 6006:6006 -it   -v ${vol}:/home/user fjrmore/pax2graphml   python3 user/test.py

```

```
#create your own script (mycript.py) , go in the directory where your script is located and execute it :.
cd script_dir
$vol= (pwd).Path
docker run -p 81:8888 -p 6006:6006 -it   -v ${vol}:/home/user fjrmore/pax2graphml  python3 mycript.py

```

How to run power-shell : see [this link](https://www.digitalcitizen.life/ways-launch-powershell-windows-admin/)


 *  PAX2GRAPHML in jupyter lab
```
$vol= (pwd).Path
docker run -p 81:8888 -p 6006:6006 -it   -v ${vol}:/home/user -e SHOW_PWD=true -e JUPYTER_PWD=password fjrmore/pax2graphml  jupyter.sh
```

then launch a browser and go to the following url : http://localhost:81/lab

and enter the password (password)

#### Installation on Windows using ubuntu terminal 

 See https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71?activetab=pivot:overviewtab

After you're environment configuration,
 follow the Docker or Conda + pip installation process (see above).


#### Installation using a Virtual Machine (Windows, MacOS, Linux)
 A pre installed VM image is available (OVA file) and has been tested with Virtual-Box. 
See the  
[VM installation  page](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/vm.md) 








​    

