# cURL  installation on MacOSX/Linux
 

cURL is a command-line tool that lets you transfer data to/from a server using various protocols.

Below you will find installion process for  MacOSX and Linux.

 

## MacOSX Installation

Enter to the computer's terminal. 

Run the command below in the terminal:
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
```
Enter your Mac's user password if needed. 


Run the command below in the terminal:
```
brew install curl
```


## Linux installation (debian/ubuntu)

Enter to the computer's terminal. 

Run the command below in the terminal:

```
sudo apt-get install curl
```
