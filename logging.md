### Logging and debugging Pax2Graphml 

The python logging system can be enabled or disabled (default) :

```
import pax2graphml as p2g
 
print("========default===========")
p2g.utils.resource_path()
print("========enable_logs===========")
p2g.enable_logs() # or p2g.verbose()
p2g.utils.resource_path()
print("======disable_logs=============")
p2g.disable_logs() 
p2g.utils.resource_path()

 
```


On some install, the python dependencies can produce unwanted logging messages :

This can be hidden by using the following environment  variable:

```
P2G_IMPORT_SILENT=true
```

