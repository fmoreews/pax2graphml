from pandas import *
import numpy as np
#from libraries.settings import *
from scipy.stats.stats import pearsonr
import itertools

df = DataFrame(np.random.random((5, 5)), columns=['gene_' + chr(i + ord('a')) for i in range(5)]) 
print(df)

correlations = {}
columns = df.columns.tolist()

print(columns)



for col_a, col_b in itertools.combinations(columns, 2):
    x=df.loc[:, col_a]
    y=df.loc[:, col_b]    
    #print("-------------------x")
    #print(x)
    #print("-------------------y")
    #print(y)
    correlations[col_a + '__' + col_b] = pearsonr(x, y)

result = DataFrame.from_dict(correlations, orient='index')
result.columns = ['PCC', 'p-value']

print(result.sort_index())


