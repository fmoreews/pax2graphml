import pandas as pd 
from sklearn.cluster import AgglomerativeClustering
from scipy.stats import pearsonr
import numpy as np

def pearson_affinity(M):
   return 1 - np.array([[pearsonr(a,b)[0] for a in M] for b in M])


def displayC(cluster):
 label = cluster.labels_
 outclust = list(zip(label, samples))  
 outclust_df = pd.DataFrame(outclust,columns=["Clusters","Samples"])  

 for clust in outclust_df.groupby("Clusters"):
    print (clust)

df = pd.io.parsers.read_table("data.txt",sep=";")
samples = df["State"].tolist()
ndf = df[["Murder", "Assault", "UrbanPop","Rape"]]
X = ndf.as_matrix()

cluster = AgglomerativeClustering(n_clusters=3, 
                               linkage='complete',affinity='euclidean').fit(X)


displayC(cluster)


cluster2 = AgglomerativeClustering(n_clusters=3, linkage='average',
                           affinity=pearson_affinity)
cluster2.fit(X)


displayC(cluster2)



