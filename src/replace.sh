
#cd /home/fmoreews/Documents/keyreg/git/KRF/spaim/graph-tool/influence2score/src

grep -lR 'paxutils' ./pax2graphml/*   | xargs sed -i 's/paxutils/pax_import/g' 
grep -lR 'paxutils' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/paxutils/pax_import/g' 
grep -lR 'paxutils' ./script/*   | xargs sed -i 's/paxutils/pax_import/g' 



grep -lR '\.annot' ./pax2graphml/*   | xargs sed -i 's/.annot/.properties/g' 
grep -lR '\.annot' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/.annot/.properties/g' 
grep -lR '\.annot' ./script/*   | xargs sed -i 's/.annot/.properties/g' 


grep -lR '\.influence_graph' ./pax2graphml/*   | xargs sed -i 's/.influence_graph/graph_explore/g' 
grep -lR '\.influence_graph' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/.influence_graph/graph_explore/g' 
grep -lR '\.influence_graph' ./script/*   | xargs sed -i 's/.influence_graph/graph_explore/g' 



grep -lR '\.spaimchecker' ./pax2graphml/*   | xargs sed -i 's/.spaimchecker/.graph_explore/g' 
grep -lR '\.spaimchecker' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/.spaimchecker/.graph_explore/g' 
grep -lR '\.spaimchecker' ./script/*   | xargs sed -i 's/.spaimchecker/.graph_explore/g' 



grep -lR 'p2ggraph_explore' ./pax2graphml/*   | xargs sed -i 's/p2ggraph_explore/p2g.graph_explore/g' 
grep -lR 'p2ggraph_explore' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/p2ggraph_explore/p2g.graph_explore/g' 
grep -lR 'p2ggraph_explore' ./script/*   | xargs sed -i 's/p2ggraph_explore/p2g.graph_explore/g' 


grep -R 'subgraph' ./pax2graphml/*   

grep -R 'subgraph' ./script/*    


grep -lR 'excludeBiopaxfileDataSources' ./pax2graphml/*   | xargs sed -i 's/excludeBiopaxfileDataSources/paxFilter/g' 
grep -lR 'excludeBiopaxfileDataSources' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/excludeBiopaxfileDataSources/paxFilter/g' 
grep -lR 'excludeBiopaxfileDataSources' ./script/*   | xargs sed -i 's/excludeBiopaxfileDataSources/paxFilter/g' 


grep -lR 'mergebiopaxfiles' ./pax2graphml/*   | xargs sed -i 's/mergebiopaxfiles/paxMerge/g' 
grep -lR 'mergebiopaxfiles' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/mergebiopaxfiles/paxMerge/g' 
grep -lR 'mergebiopaxfiles' ./script/*   | xargs sed -i 's/mergebiopaxfiles/paxMerge/g' 



grep -lR 'biopax2spaimGraphMl' ./pax2graphml/*   | xargs sed -i 's/biopax2spaimGraphMl/paxToReactionGraph/g' 
grep -lR 'biopax2spaimGraphMl' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/biopax2spaimGraphMl/paxToReactionGraph/g' 
grep -lR 'biopax2spaimGraphMl' ./script/*   | xargs sed -i 's/biopax2spaimGraphMl/paxToReactionGraph/g' 



grep -lR 'buildInfluenceGraph' ./pax2graphml/*   | xargs sed -i 's/buildInfluenceGraph/reactionToInfluenceGraph/g' 
grep -lR 'buildInfluenceGraph' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/buildInfluenceGraph/reactionToInfluenceGraph/g' 
grep -lR 'buildInfluenceGraph' ./script/*   | xargs sed -i 's/buildInfluenceGraph/reactionToInfluenceGraph/g' 

 


grep -R 'propertyValues' ./pax2graphml/*   
grep -R 'propertyValues' ../docker/pax2graphml/data/*ipynb 
grep -R 'propertyValues' ./script/* 


grep -lR 'utils.propertyValue' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.propertyValue/properties.propertyValue/g' 


grep -R 'defaultEdgeValue' ./pax2graphml/*   
grep -R 'defaultEdgeValue' ../docker/pax2graphml/data/*ipynb 
grep -R 'defaultEdgeValue' ./script/* 


grep -lR 'utils.defaultEdgeValue' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.defaultEdgeValue/properties.defaultEdgeValue/g' 




grep -R 'defaultNodeValue' ./pax2graphml/*   
grep -R 'defaultNodeValue' ../docker/pax2graphml/data/*ipynb 
grep -R 'defaultNodeValue' ./script/* 


grep -lR 'utils.defaultNodeValue' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.defaultNodeValue/properties.defaultNodeValue/g' 


grep -R 'edgePropertyValues' ./pax2graphml/*   
grep -R 'edgePropertyValues' ../docker/pax2graphml/data/*ipynb 
grep -R 'edgePropertyValues' ./script/* 



grep -R 'nodePropertyValues' ./pax2graphml/*   
grep -R 'nodePropertyValues' ../docker/pax2graphml/data/*ipynb 
grep -R 'nodePropertyValues' ./script/* 




grep -lR 'utils.loadGraph' ./pax2graphml/*   | xargs sed -i 's/utils.loadGraph/graph_explore.loadGraphml/g' 
grep -lR 'utils.loadGraph' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.loadGraph/graph_explore.loadGraphml/g' 
grep -lR 'utils.loadGraph' ./script/*   | xargs sed -i 's/utils.loadGraph/graph_explore.loadGraphml/g' 



grep -lR 'loadGraph' ./pax2graphml/*   | xargs sed -i 's/loadGraph/loadGraphml/g' 
grep -lR 'loadGraph' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/loadGraph/loadGraphml/g' 
grep -lR 'loadGraph' ./script/*   | xargs sed -i 's/loadGraph/loadGraphml/g' 



grep -lR 'utils.saveGraphml' ./pax2graphml/*   | xargs sed -i 's/utils.saveGraphml/graph_explore.saveGraphml/g' 
grep -lR 'utils.saveGraphml' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.saveGraphml/graph_explore.saveGraphml/g' 
grep -lR 'utils.saveGraphml' ./script/*   | xargs sed -i 's/utils.saveGraphml/graph_explore.saveGraphml/g' 


grep -R 'graphmlXmlString' ./pax2graphml/*   
grep -R 'graphmlXmlString' ../docker/pax2graphml/data/*ipynb 
grep -R 'graphmlXmlString' ./script/* 

grep -lR 'utils.graphmlXmlString' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.graphmlXmlString/graph_explore.graphmlXmlString/g' 


grep -R 'describeGraph' ./pax2graphml/*   
grep -R 'describeGraph' ../docker/pax2graphml/data/*ipynb 
grep -R 'describeGraph' ./script/* 


grep -lR 'utils.describeGraph' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.describeGraph/graph_explore.describeGraph/g' 


grep -R 'summary' ./pax2graphml/*   
grep -R 'summary' ../docker/pax2graphml/data/*ipynb 
grep -R 'summary' ./script/* 

grep -lR 'utils.summary' ./pax2graphml/*   | xargs sed -i 's/utils.summary/graph_explore.summary/g' 
 
grep -lR 'utils.summary' ./script/*    | xargs sed -i 's/utils.summary/graph_explore.summary/g' 



grep -R 'computeGraphMetrics' ./pax2graphml/*   
grep -R 'computeGraphMetrics' ../docker/pax2graphml/data/*ipynb 
grep -R 'computeGraphMetrics' ./script/* 

grep -lR 'utils.computeGraphMetrics' ./pax2graphml/*   | xargs sed -i 's/utils.computeGraphMetrics/graph_explore.computeGraphMetrics/g' 

grep -R 'saveOrPlot' ./pax2graphml/*   
grep -R 'saveOrPlot' ../docker/pax2graphml/data/*ipynb 
grep -R 'saveOrPlot' ./script/* 


grep -lR 'utils.saveOrPlot' ./pax2graphml/*   | xargs sed -i 's/utils.saveOrPlot/graph_explore.saveOrPlot/g' 
grep -lR 'utils.saveOrPlot' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.saveOrPlot/graph_explore.saveOrPlot/g' 
grep -lR 'utils.saveOrPlot' ./script/*   | xargs sed -i 's/utils.saveOrPlot/graph_explore.saveOrPlot/g' 



grep -R 'saveGraphImage' ./pax2graphml/*   
grep -R 'saveGraphImage' ../docker/pax2graphml/data/*ipynb 
grep -R 'saveGraphImage' ./script/* 


grep -R 'colorNodes' ./pax2graphml/*   
grep -R 'colorNodes' ../docker/pax2graphml/data/*ipynb 
grep -R 'colorNodes' ./script/* 




grep -lR 'utils.colorNodes' ./pax2graphml/*   | xargs sed -i 's/utils.colorNodes/graph_explore.colorNodes/g' 
grep -lR 'utils.colorNodes' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.colorNodes/graph_explore.colorNodes/g' 
grep -lR 'utils.colorNodes' ./script/*   | xargs sed -i 's/utils.colorNodes/graph_explore.colorNodes/g' 





grep -R 'saveImage' ./pax2graphml/*   
grep -R 'saveImage' ../docker/pax2graphml/data/*ipynb 
grep -R 'saveImage' ./script/* 




grep -lR 'utils.saveImage' ./pax2graphml/*   | xargs sed -i 's/utils.saveImage/graph_explore.saveImage/g' 
grep -lR 'utils.saveImage' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.saveImage/graph_explore.saveImage/g' 
grep -lR 'utils.saveImage' ./script/*   | xargs sed -i 's/utils.saveImage/graph_explore.saveImage/g' 



 
 subGraphListByDataSource 




grep -R 'subGraphListByDataSource' ./pax2graphml/*   
grep -R 'subGraphListByDataSource' ../docker/pax2graphml/data/*ipynb 
grep -R 'subGraphListByDataSource' ./script/* 


 
grep -lR 'properties.subGraphListByDataSource' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/properties.subGraphListByDataSource/extract.subGraphListByDataSource/g' 
 

grep -iR 'getLargestCC' ./pax2graphml/*   
grep -iR 'getLargestCC' ../docker/pax2graphml/data/*ipynb 
grep -iR 'getLargestCC' ./script/* 
 

grep -lR 'utils.saveImage' ./pax2graphml/*   | xargs sed -i 's/utils.saveImage/graph_explore.saveImage/g' 
grep -lR 'utils.saveImage' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.saveImage/graph_explore.saveImage/g' 
grep -lR 'utils.saveImage' ./script/*   | xargs sed -i 's/utils.saveImage/graph_explore.saveImage/g' 


grep -iR 'removeLargestCC' ./pax2graphml/*   
grep -iR 'removeLargestCC' ../docker/pax2graphml/data/*ipynb 
grep -iR 'removeLargestCC' ./script/* 


grep -iR 'mergeNodeByPropValues' ./pax2graphml/*   
grep -iR 'mergeNodeByPropValues' ../docker/pax2graphml/data/*ipynb 
grep -iR 'mergeNodeByPropValues' ./script/* 

grep -iR 'mergeGraphNodesByPropeties' ./pax2graphml/*   
grep -iR 'mergeGraphNodesByPropeties' ../docker/pax2graphml/data/*ipynb 
grep -iR 'mergeGraphNodesByPropeties' ./script/* 


 
grep -lR 'utils.mergeGraphNodesByPropeties' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.mergeGraphNodesByPropeties/extract.mergeGraphNodesByProperties/g' 
 
grep -iR 'mergeGraph' ./pax2graphml/*   
grep -iR 'mergeGraph' ../docker/pax2graphml/data/*ipynb 
grep -iR 'mergeGraph' ./script/* 



 
grep -lR 'utils.mergeGraph' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.mergeGraph/extract.mergeGraph/g' 
 
  
grep -iR 'mergeNodes' ./pax2graphml/*   
grep -iR 'mergeNodes' ../docker/pax2graphml/data/*ipynb 
grep -iR 'mergeNodes' ./script/* 

grep -lR 'utils.mergeNodes' ../docker/pax2graphml/data/*ipynb   | xargs sed -i 's/utils.mergeNodes/extract.mergeNodes/g' 
 
grep -iR 'copyNodeProperties' ./pax2graphml/*   
grep -iR 'copyNodeProperties' ../docker/pax2graphml/data/*ipynb 
grep -iR 'copyNodeProperties' ./script/* 


grep -lR 'utils.copyNodeProperties' ./pax2graphml/*   | xargs sed -i 's/utils.copyNodeProperties/properties.copyNodeProperties/g' 


grep -iR 'copyEdgeProperties' ./pax2graphml/*   
grep -iR 'copyEdgeProperties' ../docker/pax2graphml/data/*ipynb 
grep -iR 'copyEdgeProperties' ./script/* 


grep -lR 'utils.copyEdgeProperties' ./pax2graphml/*   | xargs sed -i 's/utils.copyEdgeProperties/properties.copyEdgeProperties/g' 

grep -iR 'ccstat' ./pax2graphml/*   
grep -iR 'ccstat' ../docker/pax2graphml/data/*ipynb 
grep -iR 'ccstat' ./script/* 


grep -iR 'nodeList' ./pax2graphml/*   
grep -iR 'nodeList' ../docker/pax2graphml/data/*ipynb 
grep -iR 'nodeList' ./script/* 


loadGraphmlml

grep -lR 'loadGraphmlml' ./pax2graphml/*   | xargs sed -i 's/loadGraphmlml/loadGraphml/g' 

grep -iR 'generateSubGraphByNodeId' ./pax2graphml/*   



# ============


grep -iR 'getSubGraphFilter' ./pax2graphml/*   
grep -iR 'getSubGraphFilter' ../docker/pax2graphml/data/*ipynb 
grep -iR 'getSubGraphFilter' ./script/* 

grep -lR 'getSubGraphFilter' ./pax2graphml/*   | xargs sed -i 's/getSubGraphFilter/subGraphFilter/g' 

grep -lR 'getSubGraphFilter' ./script/*   | xargs sed -i 's/getSubGraphFilter/subGraphFilter/g' 
 

#################################


grep -iR 'generateInfluenceSubGraph' ./pax2graphml/*   
grep -iR 'generateInfluenceSubGraph' ../docker/pax2graphml/data/*ipynb 
grep -iR 'generateInfluenceSubGraph' ./script/* 




 





