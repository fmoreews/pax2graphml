 
import os,sys
 
import numpy as np
import graph_tool.all as gt
 
 
import argparse




import pax2graphml  as p2g

#IF MODULE LAUNCH AS MAIN
if __name__ == '__main__':


    parser = argparse.ArgumentParser(description="Extract and plot distribution of CC")
    parser.add_argument('-i', action="store", dest="input_graph", help="input graphml file" )
    parser.add_argument('-o', action="store", dest="output_graph", help="output graphml file" )
    parser.add_argument('-d', action="store", dest="output_image", help="output png file" )
    args=parser.parse_args()

    input_graph="../www/graph/"+str(args.input_graph)+".graphml"
    output_graph=(None if args.output_graph is None else "../www/graph/"+str(args.output_graph)+".graphml")
    output_image=(None if args.output_image is None else "../www/pic/"+str(args.output_image)+".png")


    p2g.pax_import.prepare_spaim(input_graph,output_graph,output_image)
 


