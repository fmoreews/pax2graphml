import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import graph_tool.all as gt
from numpy.random import poisson
from scipy.stats.stats import pearsonr
from scipy.stats import normaltest
from scipy.cluster.hierarchy import linkage, to_tree
import pandas as pd
import collections
import copy
import argparse
import sys

import pax2graphml as p2g

parser = argparse.ArgumentParser(description="Extract and plot CC")
parser.add_argument('-i', action="store", dest="input_graph", help="input graphml file" )
parser.add_argument('-o', action="store", dest="output_graph", help="output graphml file" )
parser.add_argument('-d', action="store", dest="output_image", help="output png file" )
#parser.add_argument('-n', action="store", dest="iteration_count", help="number of iteration for breath traversale (minimum 2, maximum 20)" )
args=parser.parse_args()

input_graph="../www/graph/"+str(args.input_graph)+".graphml"
output_graph=(None if args.output_graph is None else "../www/graph/"+str(args.output_graph)+"_star.graphml")
output_image=(None if args.output_image is None else "../www/pic/"+str(args.output_image)+"_star.png")



"""
iteration_count=args.iteration_count

if iteration_count is None:
    iteration_count=2

iteration_count=int(nbIterations)

if nbIterations < 2:
    nbIterations=2

if nbIterations > 20:
    nbIterations=20

"""
 
class MatrixIndexBuilder:
    def __init__(self,g,wcutof=None,dbg=False):
        self.debug=dbg
        self.gr =g # input graph (influence)
        self.wcutoff=wcutof#optionally we can stop the travarsale based on weight value using self.wcutoff
        self.starGraph = gt.Graph(directed=True) # output graph with weighted edges related to entity  direct effect: a list of stars with targets as centers 
        # see https://stackoverflow.com/questions/24705644/python-dictionary-lookup-speed-with-numpy-datatypes
        self.vmapidx ={}
        self.vmapidxrev ={}
        self.visited ={}
        self.starGraph.ep.weight=self.starGraph.new_edge_property("double",val=0.0);



    
    def pruneStarByType(self):

        #FIXME with better paper understanding
        att="influenceType"
        #availability,quantity,reaction
        val="quantity"
        narr=list()   
        for v in self.starGraph.vertices():
           cval=gr.vp[att][v]
          
           keepByProp=0
           isstarcenter=0
           if cval is not None and cval==val:
             keepByProp=1
           
           for vv in v.in_neighbors():
              isstarcenter=1 
              break

           # we delete nodes that are  star centers without required  properties
           #shall we delete other nodes like neighborrs from incoming edges ???
           # see paper + AS
           if isstarcenter==1 and keepByProp==0:
               narr.append(v)
        if len(narr)>0:
           #self.starGraph.clear_vertex(narr)
           self.starGraph.remove_vertex(narr)

   
    def removeEdgesWithSmallestWeight(self,wcut):

        for v in self.starGraph.vertices():
           narr=list()    
           for e in v.in_edges():
              cval=self.starGraph.ep.weight[e]
              if  abs(cval)<wcut:
                  print(str(e))
                  narr.append(e)
           if len(narr)>0:
              for ed in narr: 
                  self.starGraph.remove_edge(ed)



    def getOrCreateStarNode(self,v):
      vindex=int(str(v))
      if vindex in self.vmapidx :
            nvindex=self.vmapidx[vindex] 
            nv=self.starGraph.vertex(nvindex)
            #print("*** %s %s %s" %(vindex,self.vmapidx[vindex], nv))
      else:
            nv=self.starGraph.add_vertex()
            nvindex=int(str(nv))
            #nvindex=self.starGraph.vertex_index[nv]
            self.vmapidx[vindex] = nvindex
            self.vmapidxrev[nvindex] = vindex
            #print("!!!!!"+str(nv)+" "+str(vindex)+" "+str(nvindex))

      return nv


 
#compute weigth and add edge in starGraph

    def computeW(self,targetn,actorn,intermedn,intermedOutEdgeCount,finalOutEdgeCount,effectA,dist,influenceType):

       
       e = self.starGraph.edge(actorn, targetn)
       #print("CW1:"+str(e))
       if e is None:
          # targetn <---e--- actorn 
          #          weight
          #print("CW1.1:")
          e=self.starGraph.add_edge(actorn, targetn) 
      
       eindex=self.starGraph.edge_index[e]
       
       we=self.starGraph.ep.weight[e]

       if effectA is None:
               print("WARNING:effectA:None %s %s %s %s " %(targetn,actorn,intermedn,influenceType))
               sys.exit()
       else:         
           if intermedn is not  None : 
             #print("CW1.2:")
             # in influence graphe : target <- intermed <- actor
             # propagation /star construction  :
             # in star graph : targetn <- actorn 

             ie = self.starGraph.edge(intermedn,targetn)
             predWeigth=self.starGraph.ep.weight[ie]
             dfactor=finalOutEdgeCount
             #dfactor=intermedOutEdgeCount

             alpha=0.005
             #V1 (FM+EB)

             #f1=(1-(alpha*dist)  ) # effect limited  by distance
             #f1=((alpha*dist) +1 ) # effect increased   by distance
             f1=1  # no effect of distance

             # super connected (hubs) have a decreased value 
             increment= np.double(predWeigth) * (f1/np.double(dfactor)) 

             increment=increment * np.double( effectA )

             #V2 (FM)
             # we can introduce a progressive cutoff , low and medium  connected nodes will have better rank
             #softcutoff=15 # below near 1 , after near (1-miniFc) (with slope)
             #miniFc=0.7 # so minimum rg for  big hubs is 0.3
             #rg=1-miniFc*p2g.math.sigmoid(dfactor-softcutoff) # rg:1->0, center softcutoff
             #increment= increment * np.double(rg) 
             #
             #idee EB :
             #sigm/ tang.arc sin
             # nb de voisins deja ds voisinage ???
             ## => need a new nodes iteration in starGraph
             # distance ? 
             #increment=increment * np.double(effectA)
             # idee de grandir tant que poids < cutoff
             # TODO  use betweenness (* 1- betw)
              
           else:
             #print("CW1.3:")
             # simple case (initilization)
             # in influence graphe : target <-  actor
             # in star graphe : targetn <- actorn
             # targetn <- actorn
             increment=np.double(effectA)
         
           
           we=we+increment
         
           self.starGraph.ep.weight[e]=we


    def defineInputList(self,att,val):
        self.inputStar=dict()
        for e in self.starGraph.edges():
           t0=e.target()
           s0=e.source()
           tx=int(str(t0))
           sx=int(str(s0))
           ti=self.vmapidxrev[tx]
           t=self.gr.vertex(ti)  

           si=self.vmapidxrev[sx]
           s=self.gr.vertex(si)

 
           trole=self.gr.vp[att][t]
           srole=self.gr.vp[att][s]
           if trole is not None and trole==val:
              if tx not in self.inputStar: 
                 self.inputStar[tx]=1
           if srole is not None and srole==val:
              if sx not in self.inputStar: 
                 self.inputStar[sx]=1 

 
    def computeScore(self):
       
        self.score=dict()
        for e in self.starGraph.edges():
           t0=e.target()
           s0=e.source()
           if t0 in self.inputStar:
             ti=self.vmapidxrev[int(str(t0))]
             t=self.gr.vertex(ti)  

             si=self.vmapidxrev[int(str(s0))]
             s=self.gr.vertex(si)
             
             
             
             tn=self.gr.vp.name[t]
             sn=self.gr.vp.name[s]
              
             sidx=int(str(s0))
             if sidx not in self.score:
                self.score[sidx]=0

             we=self.starGraph.ep.weight[e]

             #a high weight corresponds to a high specific effect
             #FIXME : if we normalize values in range ...(-1,1)  
             # we limit the specific effect of a regulator in one input entity vs  others input entities  ???
             # so we improve the input coverage effect 
             increment=p2g.math.sigmoidTransform(we)
             #increment=np.double(we)
             self.score[sidx]=self.score[sidx]+increment
        #finally, we convert all negative score to positive
        #(negative influence equals positive)   
        for sidx in self.score.keys():
            self.score[sidx]=abs(self.score[sidx])

    def starnid2label(self,snid):
            sidx=int(str(snid))
            si=self.vmapidxrev[sidx]
            #print("!!!!"+str(si))
            s=self.gr.vertex(si)
            sn=self.gr.vp.name[s]
            return sn

    def defineCoVarMatrix(self):
        matrix=dict()
        allc=dict()
        #for v in self.starGraph.vertices():
        for rowidx in self.score.keys():  
           v=self.starGraph.vertex(rowidx) 

           #rowidx=int(str(v)) 
           narr=list()
           for e in v.in_edges():
              narr.append(e)
           if len(narr)>0:
              for ed in narr: 
                  cval=self.starGraph.ep.weight[ed]
                  vv=ed.source()
                  colidx=int(str(vv))
                  k2=""+str(rowidx) #C
                  k1=""+str(colidx) #P
                  allc[k2]=True
                  if k1 in matrix.keys():
                     col=matrix[k1]
                  else:
                     col=dict()
                  col[k2]=cval 

                  matrix[k1]=col
        # we add missing values and convert to list
        for k1 in matrix.keys():             
            col=matrix[k1]
            for k2 in  allc.keys():
              if k2 not in col:
                 col[k2]=0 # not np.nan , try random large values?

            matrix[k1]=list(col.values())

        print(matrix)
        correlations = {}
        cormat={}
        label={} 
        i=-1
        for k1 in matrix.keys():           
            i+=1
            j=-1
 
            label[i]=self.starnid2label(k1)
            if i not in cormat:
                    cormat[i]=dict()
            #        label[i]=dict()
            for k1b in matrix.keys(): 
              j+=1
              
              if k1 != k1b: 
                x=matrix[k1]
                y=matrix[k1b]

                normx=self.normtest(x)
                normy=self.normtest(y)
                k=str(k1)+ '_' +str(k1b)
                p = pearsonr(x, y)
                correlations[k] =p

                
                n=normx*normy
                correlations[k]=correlations[k]+ (n,) # add tuple element
                c=p[0]
              else:
                #c=np.nan
                c=0  
              cormat[i][j]=c
              #label[i][j]=[k1,k1b]
              



        result = pd.DataFrame.from_dict(correlations, orient='index')
        result.columns = ['pearsoncc', 'p-value','norm']
        print(result.sort_index())
        p2g.clust.clustering(cormat,label)    




              
    def normtest(self,x):
       k2, p = normaltest(x)
       alpha = 1e-3
       #print("p = {:g}".format(p))

       if p < alpha:  # null hypothesis: x comes from a normal distribution
          #print("The null hypothesis can be rejected:  Data do not follow a normal distribution")
          return 0
       else:
          #print("The null hypothesis cannot be rejected: Cannot conclude. Can be normal")
          return 1



    def displayScore(self):
        print("*********************")
        print("scores:")
 
        sorted_x = sorted(self.score.items(), key=lambda kv: kv[1], reverse=True)
        sorted_dict = collections.OrderedDict(sorted_x)
        for sidx in sorted_dict.keys():
             s0=self.starGraph.vertex(sidx)
             si=self.vmapidxrev[sidx]
             s=self.gr.vertex(si)
             sn=self.gr.vp.name[s]
             sc=self.score[sidx]
             print("%s = %s" %(sn,sc))

        print("*********************")

    def displayWfull(self):
        for e in self.starGraph.edges():
           t0=e.target()
           s0=e.source()

           ti=self.vmapidxrev[int(str(t0))]
           t=self.gr.vertex(ti)  

           si=self.vmapidxrev[int(str(s0))]
           s=self.gr.vertex(si)


           we=self.starGraph.ep.weight[e]
           tn=self.gr.vp.name[t]
           sn=self.gr.vp.name[s]
           print("%s --(%s)--> %s" %(sn,we,tn))

           
          # print(to_string(self.gr,t))




    def displayWHisto(self):
        lst=list()

        for we in self.starGraph.ep.weight:
           #print("==>%s " %(str(we)))
           lst.append(we)
        ar=np.array(lst)
        sl=np.sort(np.array(ar))
        un, occ = np.unique(ar, return_counts=True)
        lz=zip(un, occ) 
        for el in lz:
            s=el[0]
            oc=el[1]
            print(str(s)+"="+str(oc), end="\n")



    def iterateNodesCW(self,targetn,sourcen,dist):
            #print("iterateNodesCW:1: %s<=%s"  %(targetn,sourcen))
            #star center
            starnv=self.getOrCreateStarNode(targetn)

            #print("==>"+to_string(v))
            #print("==>startnode:%s \t%s" %(v, self.gr.vp["influenceType"][v]))

            if sourcen is None:
                #print("iterateNodesCW:init")
                #node sourcen is None at  initialization
                # we create an edge for each original node using neighborhood N1
                vv=targetn
                intermedstarsourcen=None
                interOutEdgeCount=None
            else:
                #print("iterateNodesCW:intermed")
                # we create edges using using  neighborhood N > 1
                # => star construction

                intermedstarsourcen=self.getOrCreateStarNode(sourcen)
                #count out edges from influence graph
                interOutEdgeCount=p2g.utils.count_edge(sourcen,"out")

                vv=sourcen
            
            for e, w in zip(vv.in_edges(), vv.in_neighbors()):
                effectA=self.gr.ep["effect"][e]
                
                influenceType=self.gr.vp["influenceType"][w]
                wOutEdgeCount=p2g.utils.count_edge(w,"out")
                #print("\tneigh:%s \tinf:%s\teffect:%s" %(w, influenceType, effectA))

                starnw=self.getOrCreateStarNode(w)

               
                self.computeW(starnv,starnw,intermedstarsourcen,interOutEdgeCount,wOutEdgeCount,effectA,dist,influenceType)





    def getVisited(self,eid):
       vis=False
       if eid in self.visited:
          vis=self.visited[eid]
       return vis
    
   
    def setVisited(self,eid,val):
       self.visited[eid]=val

    def copyProp2Star(self):
      self.starGraph.vp.iid=self.starGraph.new_vertex_property("int");
      for stv in self.starGraph.vertices():
             stindex=int(str(stv))
             grindex=self.vmapidxrev[stindex]
             grnode=self.gr.vertex(grindex)
             grindex=int(str(grnode))
             #we create attribute that map the influence grpah node if 
             #in  the output star Graph 
             self.starGraph.vp["iid"][stv]=grindex
             #for prop in ["",""] 
             #   self.starGraph.vp[prop][stv]=self.gr.vp[prop][grnode]



    def computeStarGraph(self):
      ct=0 
      iterct=100 # to avoid infinite loop

      #initialization of starGraph
      for v in self.gr.vertices():

        doproc=True

        #print("==>%s %s %s %s %s" %(ct,  str(v)  , att,cval,val  )  )
        #att="influenceType"
        #val="quantity",
        #cval=self.gr.vp[att][v]
        #if cval is not None and cval==val:
        #    doproc=True

        if doproc==True :   
            ct+=1
            if self.debug==True and ct>100:
                break

            #starGraph initialization with neighbors N=1 and singletons
            self.iterateNodesCW(v,None,1)

      # iterations => we manage a maximum distance of N=iterct (=max(i)+1) beetween sources sand targets
      for i in range(1, iterct-1):  
        asElement=False
        dist=i+1
        cts=0  
        print("==i=%s => neigh %s" % (i,i+1)) 
        # we iterate on edges of the output starGraph


        narr=list()
        for starNode in self.starGraph.vertices():
            narr.append(starNode)    
        #print("=====narr:0 %s" % (len(narr)))   

        for starNode in narr:
            
          cts+=1  
          if self.debug==True and cts>100:
              break

          edgel=list()
          for edgeStar in  starNode.in_edges():
             eid=int(self.starGraph.edge_index[edgeStar]) 
             if self.getVisited(eid)==False:
                #src=regulator
                src=edgeStar.source()
                #trg=regulated (center of star)
                trg=edgeStar.target()
                doadd=True
                if self.wcutoff is not None:
                    we=self.starGraph.ep.weight[edgeStar]
                    if we < self.wcutoff:
                        doadd=False

                if doadd==True:    
                   asElement=True
                   edgel.append([src,trg,eid])
 

                #print("nei:%s;n%s" % (i+1,starNode))
          #we can not create edges in during edge iteration starNode.in_edges() =>  edges in edgel
          for el in edgel:
             src=el[0]
             trg=el[1]
             eid=el[2]
             #print("==edgeStar:1 %s <= %s" % (src,trg))
             # the center of each start is the target node
             # src1->trg;  src2->trg; src3->trg; ...
             #propagation is enabled because we select the neighbors from the sources
             sindex=int(str(src))
             gsrcindex=self.vmapidxrev[sindex]
             #srcNode is the node from input self.gr (influence) corresponding to the src node (src is in the star graph)
             gsrcNode=self.gr.vertex(gsrcindex)

             tindex=int(str(trg))
             gtrgindex=self.vmapidxrev[tindex]
             gtrgNode=self.gr.vertex(gtrgindex)
             
             self.iterateNodesCW(gtrgNode,gsrcNode,dist)
             self.setVisited(eid,True)

        if asElement==False:
           print("end iteration")
           break

      #associated source graphe node ids to output
      self.copyProp2Star()



debug=False # warning very partial traversale if True

#g=None
g=p2g.graph_explore.load_graphml(input_graph)
print(p2g.graph_explore.summary(g))


wcutoff=None

cw= MatrixIndexBuilder(g,wcutoff,debug)


cw.computeStarGraph()

cw.removeEdgesWithSmallestWeight(0.01)
 
cw.displayWHisto()
cw.displayWfull()
print(p2g.graph_explore.summary(g))

cw.defineInputList("role","input")
cw.computeScore()
cw.displayScore()
cw.defineCoVarMatrix()

#########todo: in starGarph, filter (central ?) nodes  on influenceType 
#availability,quantity,reaction
# => implement pruneStar()
#cw.pruneStar()
#########################
##id:
##poids : specificite
##score +couverture
##fct score
#########################
 

p2g.graph_explore.save_graphml(cw.starGraph, output_graph)
 


print("end_star_generation")

