import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import graph_tool.all as gt
from numpy.random import poisson
import pandas as pd
import argparse


import pax2graphml as krf

parser = argparse.ArgumentParser(description="Extract and plot CC")
parser.add_argument('-i', action="store", dest="input_graph", help="input graphml file" )
parser.add_argument('-o', action="store", dest="output_graph", help="output graphml file" )
parser.add_argument('-d', action="store", dest="output_image", help="output png file" )
args=parser.parse_args()

input_graph="../www/graph/"+str(args.input_graph)+".graphml"
output_graph=(None if args.output_graph is None else "../www/graph/"+str(args.output_graph)+".graphml")
output_image=(None if args.output_image is None else "../www/pic/"+str(args.output_image)+".png")


g=krf.graph_explore.load_graphml(input_graph)


iteration_count=args.iteration_count
targets=[0,1,2,4]



for t in targets:
    neighbourhood=[t]
    newNeighbours=neighbourhood

    for i in range(1, int(iteration_count)+1):
        newNeighboursTamp=newNeighbours
        newNeighbours=[]
        for v in newNeighboursTamp:
            for n in g.get_in_neighbours(v):
                if (n not in neighbourhood):
                    newNeighbours.append(n)


                    """
                    COMPUTE
                    degree=degreeMap[n]
                    giveScoreToNode(n, i, degree)
                    """



        if (newNeighbours==[]):
            break
        newNeighbours=np.unique(newNeighbours)
        neighbourhood=np.concatenate((neighbourhood, newNeighbours))
        newNeighboursTamp=newNeighbours

 
 
if output_graph is not None:
       p2g.graph_explore.save_graphml(infG, output_graph)
if output_image is not None:
       p2g.graph_explore.save_image(infG, output_image)


