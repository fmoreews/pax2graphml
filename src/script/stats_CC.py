import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import graph_tool.all as gt
from numpy.random import poisson
import pandas as pd
import argparse
import pickle


#DISTRIBUTION NODES IN CC
def plotDistributionOfNodesInCC(g):
	comp, histCC = gt.label_components(g, directed=False)
	hist2=[]
	for sizeCC in histCC:
		hist2+=([(sizeCC)]*sizeCC)
	dist = pd.DataFrame(hist2, columns=["influence BioPAX"])
	ax=dist.plot.hist(dist, title="Distribution of entities in CC", bins=np.logspace(0, 6, 100), ylim=(0,100), logx=True, weights=np.ones_like(dist[dist.columns[0]])*100./len(dist))
	ax.set_xlabel('CC order')
	ax.set_ylabel('% of entities')



#ORDER CC DISTRIBUTION
def plotDistributionOfCCOrder(g):
	comp, histCC = gt.label_components(g, directed=False)
	dist = pd.DataFrame(histCC, columns=["influence BioPAX"])
	ax=dist.plot.hist(dist, title="CC order distribution", bins=np.logspace(0, 6, 100), loglog=True, bottom=0, edgecolor='black', linewidth=0.5,)
	ax.set_xlabel('CC order')
	ax.set_ylabel('amount of CC')



#DEGREE LCC DISTRIBUTION
def plotDistributionOfDegreeInLCC(g):
	lCCFilt = gt.label_largest_component(g, directed=False)
	lCC = gt.GraphView(g, vfilt=lCCFilt)
	lCC = gt.Graph(lCC, prune=True)
	degreeMap=lCC.degree_property_map("total")
	dist = pd.DataFrame(degreeMap.a, columns=["influence BioPAX"])
	ax=dist.plot.hist(dist, title="largest CC degree distribution", bins=np.logspace(0, 5, 100), loglog=True, bottom=0, edgecolor='black', linewidth=0.5,)
	ax.set_xlabel('degree')
	ax.set_ylabel('amount of nodes')



#SPAIM TYPE DISTRIBUTION
def plotDistributionOfSpaimType(g):
	spaimHist={'s':0,'p':0,'a':0,'i':0,'m':0}
	for e in g.edges():
		spaimHist[g.ep.spaim[e]]+=1
	hist=[value for value in spaimHist.values()]

	plt.bar(np.arange(5), hist, tick_label=['s','p','a','i','m'])
	plt.title('SPAIM types distribution')
	plt.xlabel('spaim type')
	plt.ylabel('amount of edges')




#SAVE BETWEENNESS AND CLOSENESS
def saveBetweennessAndCloseness(g):
	lCCFilt = gt.label_largest_component(g, directed=False)
	lCC = gt.GraphView(g, vfilt=lCCFilt)
	lCC = gt.Graph(lCC, prune=True)
	vpBetween = gt.betweenness(lCC)[0]
	vpClose = gt.closeness(lCC)

	with open("vpBetween_infBioPAX.txt", "wb") as fp:   #Pickling
	    pickle.dump(vpBetween.a, fp)
	with open("vpClose_infBioPAX.txt", "wb") as fp2:   #Pickling
	    pickle.dump(vpClose.a, fp2)

def loadBetweennessAndCloseness():
	with open("vpBetween_infBioPAX.txt", "rb") as fp:   #Unpickling
		betweenness=pickle.load(fp)
	with open("vpBetween_infBioPAX.txt", "rb") as fp2:   #Unpickling
		closeness=pickle.load(fp2)
	return betweenness, closeness




#BETWEENNESS AND GROS GRAIN STATS FOR ALL CC
def writeBetweennessAndGrosGrain(g, vpBetween, vpClose):
	g.set_directed(False)
	comp, histCC = gt.label_components(g, directed=False)

	betweennessAllCC = open("../www/txt/stats_infBioPAX/betweennessAllCC_infBioPAX.txt", "w")
	grosGrainAllCC = open("../www/txt/stats_infBioPAX/grosGrainAllCC_infBioPAX.txt", "w")
	betweennessAllCC.write("CC_id")
	nbBins=100
	rangeMin=0.
	rangeMax=1.
	for x in np.linspace(rangeMin, rangeMax-(rangeMax-rangeMin)/nbBins, nbBins):
		betweennessAllCC.write("\t%s" %x)
	betweennessAllCC.write("\tmax_degree_node\n")
	grosGrainAllCC.write("CC_id\t|V|\t|E|\tdensity\tdiameter\n")

	for CCi in range(len(histCC)):
		#traitements composantes par composantes
		vCCFilt=list(comp.a==CCi)
		CC = gt.GraphView(g, vfilt=vCCFilt)
		CC = gt.Graph(CC, prune=True)

		order=len(list(CC.vertices()))
		size=len(list(CC.edges()))
		print("\nCC"+str(CCi)+" order: "+str(order))
		print("CC"+str(CCi)+" size: "+str(size))
		if (order>1):
			density=2*size/(order*(order-1))
		else:
			density="undef"
		diameter=gt.pseudo_diameter(CC)[0]

		if (CCi!=0):
			vpBetween = gt.betweenness(CC)[0].a
			vpClose = gt.closeness(CC).a

		#Informations gros grain
		grosGrainAllCC.write("CC_%s\t%s\t%s\t%s\t%s\n" %(CCi, order, size, density, diameter))

		#pour chaque CC, calcul de la distribution du betweenness
		barValues, bin_edges= np.histogram(vpBetween, bins=nbBins, range=(rangeMin, rangeMax))
		betweennessAllCC.write("CC_%s" %CCi)
		for b in barValues:
			betweennessAllCC.write('\t%s' %b)
		#ajoute le nom du noeud de plus grand degré de la CC
		betweennessAllCC.write('\t'+CC.vp.name[np.argmax(CC.degree_property_map("total").a)]+'\n')

		#pour chaque CC, créé un fichier détaillant chaque noeuds (id, degré, betweenness et closeness)
		detailsCC = open("../www/txt/stats_infBioPAX/detailsCC_infBioPAX/detailsCC_"+str(CCi).zfill(4)+".txt", "w")
		detailsCC.write("id\tname\tdegree\tbetweenness\tcloseness\n")
		for v in CC.vertices():
			detailsCC.write(CC.vp._graphml_vertex_id[v]+'\t'+CC.vp.name[v]+"\t%s\t%s\t%s\n" %(CC.degree_property_map("total")[int(v)], vpBetween[int(v)], vpClose[int(v)]))
		detailsCC.close()

	betweennessAllCC.close()
	grosGrainAllCC.close()


#LCC ONLY
##100 HIGHEST DEGREE NODES LIST
def writeHighestDegreeNodes(g):
	highestDegreeNodes = open("../www/txt/stats_infBioPAX/highestDegreeNodesLCC_infBioPAX.txt", "w")
	highestDegreeNodes.write('id\tname\tdegree\n')

	lCCFilt = gt.label_largest_component(g, directed=False)
	lCC = gt.GraphView(g, vfilt=lCCFilt)
	lCC = gt.Graph(lCC, prune=True)
	degreeMap=lCC.degree_property_map("total")

	nbHighestDegreeNodes=100
	for i in range(nbHighestDegreeNodes):
		vMax=np.argmax(degreeMap.a)
		highestDegreeNodes.write(str(lCC.vp._graphml_vertex_id[vMax])+'\t'+str(lCC.vp.name[vMax])+'\t'+str(degreeMap[vMax])+'\n')
		degreeMap[vMax]=0
	highestDegreeNodes.close()





##REMOVE PROGRESSIVELY HIGHEST DEGREE NODES AND PLOT AMOUNT OF CC AND DIAMETER OF LCC
def plotRemoveHighestDegreeNodes_amountOfCC_diameterOfLCC(g, displayProgress=False):
	lCCFilt = gt.label_largest_component(g, directed=False)
	lCC = gt.GraphView(g, vfilt=lCCFilt)
	lCC = gt.Graph(lCC, prune=True)
	degreeMap=lCC.degree_property_map("total").a
	nbNodeToRemove=100

	amountOfCC=[]
	diameters=[]
	for i in range(nbNodeToRemove):

		if (displayProgress):
			print(str(i)+" / "+str(nbNodeToRemove)+" nodes removed")

		vMax=np.argmax(degreeMap)
		comp, orderCC = gt.label_components(lCC, directed=False)
		amountOfCC.append(len(orderCC))

		newLCC=gt.Graph(gt.GraphView(lCC, vfilt=gt.label_largest_component(lCC, directed=False)), prune=True)
		diameter = gt.pseudo_diameter(newLCC)[0]
		diameters.append(diameter)

		lCC.remove_vertex(vMax)
		degreeMap[vMax]=0

	x=np.arange(nbNodeToRemove)
	plt.plot(x, diameters, color="r")
	plt.ylim(ymin = -0.5, ymax=max(diameters)*1.1)
	plt.title("diameter of the largest CC")
	plt.savefig("../www/pic/removingHighestDegreeNodes_diameterOfLCC_spaimBioPAX.png")
	plt.close()

	plt.plot(x, amountOfCC, color="orange")
	plt.ylim(ymin = -0.5)
	plt.title("amount of CC")
	plt.savefig("../www/pic/removingHighestDegreeNodes_amountOfCC_spaimBioPAX.png")
	plt.close()




##Affichage de l'histogramme des betweenness
def plotBetweennessHistogram(g):
	lCCFilt = gt.label_largest_component(g, directed=False)
	lCC = gt.GraphView(g, vfilt=lCCFilt)
	lCC = gt.Graph(lCC, prune=True)
	vpBetween = gt.betweenness(lCC)[0]
	nbBins=100
	rangeMin=0.
	rangeMax=1.
	dist = pd.DataFrame(vpBetween.a, columns=["spaim BioPAX"])
	ax=dist.plot.hist(dist, title="largest CC betweenness distribution", bins=np.linspace(0, 1, 100), logy=True, bottom=0, edgecolor='black', linewidth=0.5,)
	ax.set_xlabel('betweenness')
	ax.set_ylabel('amount of nodes')







#IF MODULE LAUNCH AS MAIN
if __name__ == '__main__':

	import krf.utils
	parser = argparse.ArgumentParser(description="Extract and plot distribution of CC")
	parser.add_argument('-i', action="store", dest="input_graph", help="input graphml file" )
	parser.add_argument('-o', action="store", dest="output_graph", help="output graphml file" )
	parser.add_argument('-d', action="store", dest="output_image", help="output png file" )
	args=parser.parse_args()
	print("\n%s\n" %args)

	input_graph="../www/graph/"+str(args.input_graph)+".graphml"
	output_graph=(None if args.output_graph is None else "../www/graph/"+str(args.output_graph)+".graphml")
	output_image=(None if args.output_image is None else "../www/pic/"+str(args.output_image)+".png")

	g=krf.graph_explore.load_graphml(input_graph, directed=False)

	"""
		INSERT FONCTIONS
	"""
	g.list_properties()


	if (output_image is not None):
		plt.savefig("../www/pic/"+args.output_image+".png")
