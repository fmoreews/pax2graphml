import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import graph_tool.all as gt
import pandas as pd
import argparse

import pickle

import pax2graphml as p2g
for thing in dir(p2g): print (thing)


parser = argparse.ArgumentParser(description="Extract and plot CC")
parser.add_argument('-i', action="store", dest="input_graph", help="input graphml file" )
parser.add_argument('-o', action="store", dest="output_graph", help="output graphml file" )
parser.add_argument('-d', action="store", dest="output_image", help="output png file" )
args=parser.parse_args()

input_graph="../www/graph/"+str(args.input_graph)+".graphml"
output_graph=(None if args.output_graph is None else "../www/graph/"+str(args.output_graph)+".graphml")
output_image=(None if args.output_image is None else "../www/pic/"+str(args.output_image)+".png")

print('\n')
print(args)



g=p2g.graph_explore.load_graphml(input_graph, directed=True)
# lCC= p2g.extract.largest_connected_component(g, directed=False)
# g = p2g.extract.subgraph_by_direction(g, 2, chosen_node_id=91304, direction="in", neighbour_count=3)
g = p2ggraph_explore.reaction_to_influence_graph(g)


if output_graph is not None:
       p2g.graph_explore.save_graphml(g, output_graph)
if output_image is not None:
       p2g.graph_explore.save_image(g, output_image)


