import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import graph_tool.all as gt
from numpy.random import poisson
import pandas as pd
import argparse
 
from pax2graphml import utils
from pax2graphml import properties
from pax2graphml import graph_explore
from pax2graphml import extract

 


 
 
if __name__ == '__main__':

 
    parser = argparse.ArgumentParser(description="Extract and plot distribution of CC")
    parser.add_argument('-i', action="store", dest="input_graph", help="input graphml file" )
    parser.add_argument('-o', action="store", dest="output_graph", help="output graphml file" )
    parser.add_argument('-d', action="store", dest="output_image", help="output png file" )
    parser.add_argument('-m', action="store", dest="nbIterations", help="neighbourhod max distance" )
    parser.add_argument('-c', action="store", dest="chosen_node_id", help="choose a central node for neighbourhod (chosen randomly by default)" )
    parser.add_argument('-a', action="store", dest="direction", help="get {all, in , out} neighbours (all by default)" )
    parser.add_argument('-l', action="store", dest="node_limit", help="cardinality limit (approximatively)" )
    parser.add_argument('-n', action="store", dest="neighbour_count", help="number of new neighbours at each iteration (approximatively)" )
    args=parser.parse_args()
    print("\n%s\n" %args)

    input_graph=str(args.input_graph)
    output_graph=(None if args.output_graph is None else +str(args.output_graph))
    output_image=(None if args.output_image is None else str(args.output_image))

    g=graph_explore.load_graphml(input_graph)
    subGraph=graph_explore.subgraph_by_direction(g, args.nbIterations, args.chosen_node_id, args.direction, args.node_limit, args.neighbour_count)

    if output_graph is not None:
       graph_explore.save_graphml(subGraph, output_graph)
    if output_image is not None:
       graph_explore.save_image(subGraph, output_image)




