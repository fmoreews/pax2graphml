import pax2graphml  as p2g
import timeit
import os.path
rootPath="/home/user/"
srcDir=rootPath+"work/"
resultPath=rootPath+"work/"
biopaxInputfile=srcDir+"PathwayCommons12.All.BIOPAX.owl"
biopaxOutputFile= resultPath+"pc12_filtered.owl"  
#graphmlfileIn=resultPath+"merged_small.graphml"
#graphmlfileOut=resultPath+"merged_small_filtered.graphml"

filter=["mirtarbase","ctd"]

#filter=["localbase"]
#example datasources
# bind biogrid corum ctd dip drugbank hprd humancyc innatedb 
# inoh intact intact_complex kegg mirtarbase msigdb netpath 
# panther pid  reconx smpdb wp psp reactome 
dowait=True
#warning large file, can be slow
if dowait==True:
  start = timeit.default_timer()
  response=p2g.pax_import.biopax_filter(biopaxInputfile,filter,biopaxOutputFile)
  print(response)
  stop = timeit.default_timer()
  print("Time: %.2f s"%(  stop - start) ) 


