import pax2graphml as p2g

import os 
import timeit

rootPath="/home/user/"
srcDir=rootPath+"work/"
resultPath=rootPath+"work/"
spaimfile=resultPath+"pc12_filtered.graphml"
influencefile=resultPath+"pc12_filtered_influence.graphml"
spaimfilechecked=resultPath+"temp_checked.graphml"
start = timeit.default_timer()


p2g.pax_import.prepare_spaim(spaimfile,spaimfilechecked,None)
p2g.pax_import.influence_graph(spaimfilechecked,influencefile,None)


os.remove(spaimfilechecked)
stop = timeit.default_timer()
print("Time: %.2f s"%(  stop - start) )

