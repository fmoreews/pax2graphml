import pax2graphml  as p2g
import timeit
import os.path
rootPath="/home/user/"
srcDir=rootPath+"work/"
resultPath=rootPath+"work/"
biopax_file= resultPath+"pc12_filtered.owl"  
graphml_file=resultPath+"pc12_filtered.graphml"


start = timeit.default_timer()
response=p2g.paxutils.biopax_to_reaction_graph(biopax_file,graphml_file)
print(response)
stop = timeit.default_timer()
print("Time: %.2f s"%(  stop - start) ) 


