 Data related to paper revisions. april 2021





**content**

control_update.pdf : step by step explanation of the control mechanism representation update

G6P.owl  : G6P BIOPAX input example file

G6P-V1.1.graphml  : new PAX2GRAPHML graphml output  

G6P_cytoscape.pdf : view of   G6P-V1.1.graphml imported in Cytoscape 

graph_yed_style.graphml : 	data for revision explanation 

G6P_yed.pdf  :  view of   graph_yed_style.graphml imported in yEd

biopax_class_relations_diag.pdf : view of BIOPAX classes relations 

compatibility_yed.pdf : description of the code update related to compatibility wit the yEd graph editor