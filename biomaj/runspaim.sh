#!/bin/bash

. /softs/local/env/envsingularity-3.5.2.sh

IMAGENAME=fjrmore/pax2graphml

WORKDIR=$1
WORKFILE=$2

FILE=`head -n ${SLURM_ARRAY_TASK_ID} $WORKFILE | tail -n  1`
echo "Spaiming $FILE"
SCRIPT=/db/biomaj/biomaj-process/pax2graphml/spaim.py

singularity run -B /db:/db docker://$IMAGENAME python3 $SCRIPT $WORKDIR $FILE
