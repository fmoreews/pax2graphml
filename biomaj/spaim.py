import pax2graphml  as p2g
import timeit
import os.path
import sys

p2g.enable_logs()

#p2g.utils.__P2GJMEM=" -Xmx148g  -XX:+UseConcMarkSweepGC  "
p2g.utils.defineXmx("148g")


resultPath=sys.argv[1]
biopaxfile=sys.argv[2]
graphmlfile=os.path.join(resultPath, os.path.basename(biopaxfile).replace('.owl', '') +".graphml")

start = timeit.default_timer()
response=p2g.pax_import.biopax_to_reaction_graph(biopaxfile,graphmlfile)
print(response)
stop = timeit.default_timer()
print("Time: %.2f s"%(  stop - start) ) 


