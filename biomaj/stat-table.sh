#!/bin/bash

. /softs/local/env/envsingularity-3.5.2.sh

IMAGENAME=fjrmore/pax2graphml

WORKDIR=$1 # generated  regulation GRAPHML files directory 
PCVERSION="12" # pathway commons version
DOEXTRACTINFO="0" # "1" for extracting metadata (from urls) , "0" disabled
OPT=" $WORKDIR $PCVERSION $DOEXTRACTINFO"
singularity run -B /db:/db docker://fjrmore/pax2graphml python3 /db/biomaj/biomaj-process/pax2graphml/stat-table.py $OPT

DBFILE=bd.html
WEBDIR=/web/genosites/specific/pax2graphml
DBFILELOC=$WEBDIR/db.html
DBFILELOCSAV=$WEBDIR/db.sav.html
cp $DBFILELOC $DBFILELOCSAV
cp $DBFILE $DBFILELOC
