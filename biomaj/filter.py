import pax2graphml  as p2g
import timeit
import os
import sys

p2g.enable_logs()


#p2g.utils.__P2GJMEM=" -Xmx148g  -XX:+UseConcMarkSweepGC  "
p2g.utils.defineXmx("148g")


resultPath=sys.argv[1]
biopaxInputfile=sys.argv[2]
biopaxOutputFile= os.path.join(resultPath, "pc_filtered.owl")

filter=["mirtarbase","ctd"]

#filter=["localbase"]
#example datasources
# bind biogrid corum ctd dip drugbank hprd humancyc innatedb 
# inoh intact intact_complex kegg mirtarbase msigdb netpath 
# panther pid  reconx smpdb wp psp reactome 
dowait=True
#warning large file, can be slow
if dowait==True:
  start = timeit.default_timer()
  response=p2g.pax_import.biopax_filter(biopaxInputfile,filter,biopaxOutputFile)
  print(response)
  stop = timeit.default_timer()
  print("Time: %.2f s"%(  stop - start) ) 

