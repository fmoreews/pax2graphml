#!/bin/bash

/db/biomaj/biomaj-process/slurm.sh

workdir=$datadir/$dirversion/$localrelease
if [ "a${datadir}" == "a" ]; then
    workdir=/db/pathwaycommons/current
fi

#rm -f $workdir/flat/*Warehouse*

#mkdir -p $workdir/graphml
#mkdir -p $workdir/graphml_in
#cp $workdir/flat/*.owl $workdir/graphml_in/
#rm $workdir/graphml_in/*All*

cd $workdir/flat
# runfilter.sh filter file All to $workdir/graphml_in
#sbatch --wait --cpus-per-task=2 --mem=60G /db/biomaj/biomaj-process/pax2graphml/runfilter.sh $workdir/graphml_in $workdir/flat/*All*.owl
#sbatch --workdir $workdir/graphml_in --cpus-per-task=2 --mem=60G /db/biomaj/biomaj-process/pax2graphml/runfilter.sh $workdir/graphml_in $workdir/flat/*All*.owl

# runspaim.sh
rm -f $workdir/graphml_in/graphml.out
ls -1 $workdir/graphml_in/*.owl > $workdir/graphml_in/graphml.out
nbfiles=`wc -l $workdir/graphml_in/graphml.out | awk '{ print $1 }'`
#sbatch --wait --workdir $workdir/graphml_in --cpus-per-task=2 --mem=60G --array=1-${nbfiles}%5 /db/biomaj/biomaj-process/pax2graphml/runspaim.sh $workdir/graphml $workdir/graphml_in/graphml.out 
sbatch --workdir $workdir/graphml_in --cpus-per-task=2 --mem=60G --array=1-${nbfiles}%5 /db/biomaj/biomaj-process/pax2graphml/runspaim.sh $workdir/graphml $workdir/graphml_in/graphml.out

#rm -f $workdir/graphml_in/*.owl
