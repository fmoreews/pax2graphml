#!/bin/bash

/db/biomaj/biomaj-process/slurm.sh

workdir=$datadir/$dirversion/$localrelease
if [ "a${datadir}" == "a" ]; then
    workdir=/db/pathwaycommons/current
fi

rm -f $workdir/flat/*Warehouse*

mkdir -p $workdir/graphml
mkdir -p $workdir/graphml_in
cp $workdir/flat/*.owl $workdir/graphml_in/
rm $workdir/graphml_in/*All*

cd $workdir/flat
# runfilter.sh filter file All to $workdir/graphml_in
sbatch --wait --workdir $workdir/graphml_in --cpus-per-task=2 --mem=148G /db/biomaj/biomaj-process/pax2graphml/runfilter.sh $workdir/graphml_in $workdir/flat/*All*.owl


# runspaim.sh
rm -f $workdir/graphml_in/graphml.out
ls -1 $workdir/graphml_in/*.owl > $workdir/graphml_in/graphml.out
nbfiles=`wc -l $workdir/graphml_in/graphml.out | awk '{ print $1 }'`
# up to 20 days
sbatch --wait --workdir $workdir/graphml_in --time 20-00 --cpus-per-task=2 --mem=148G --array=1-${nbfiles}%5 /db/biomaj/biomaj-process/pax2graphml/runspaim.sh $workdir/graphml $workdir/graphml_in/graphml.out

rm -f $workdir/graphml_in/*.owl

# compress graphml files
graphmldir="$workdir/graphml"
destdir="$workdir/graphml_compressed"
mkdir -p $destdir
for filename in $graphmldir/*.graphml; do
        filen="$(basename "$filename").gz"
        destfile="$destdir/$filen"
        echo "gzip <  $filename >  $destfile"
done
