#!/bin/bash

 
graphmldir=$1
destdir=$1"_compressed"
mkdir -p $destdir
for filename in $graphmldir/*.graphml; do
        filen="$(basename "$filename").gz"
        destfile="$destdir/$filen"
        echo "gzip <  $filename >  $destfile"
        gzip <  "$filename" >  "$destfile"
done
