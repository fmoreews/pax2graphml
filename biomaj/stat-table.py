import glob
import os,sys,re
import os.path, time
import requests, csv, json
from jinja2 import Environment, FileSystemLoader
import pax2graphml  as p2g

processmode=2
doarg=True
datadir="/home/user/example/result/"
templatedir="/home/user/example/dev/"
#generate infofile, json file with metadata (from urls urlinfo and srcurl)
doextractinfo=True # if true regenerate info.json
pcv="12" # current pathway commons version
if doarg==True:
  datadir=sys.argv[1]
  pcv=sys.argv[2]
  doextractinfo=bool(int(sys.argv[2]))
  templatedir=os.getcwd()

#biopaxOutputFile= os.path.join(resultPath, "pc_filtered.owl")
dd=datadir
if dd[-1] == '/':
   dd=dd[:-1]
datadirgz=dd+"_compressed"

print(datadir)
print(datadirgz)
urlinfo="https://www.pathwaycommons.org/archives/PC2/v"+pcv+"/datasources.txt"
srcurl="https://www.pathwaycommons.org/archives/PC2/v"+pcv+"/"
prefix="PathwayCommons"+pcv+"."
infofile="info.json"
rowfile="rows.json"

baseurl="/db"


hardlabel={"pc_filtered":{"pos":2},"detailed":{"pos":3}}

dmap= {

  "detailed": 
    {
    "desc":"All Pathway Commons BIOPAX sources"
    }
    , 

  "pc_filtered": 
    {
    "desc":"merged regulation banks (reactome,pid,kegg,humancyc,inoh,intact_complex,corum,msigdb,netpath,panther,pathbank,psp,reconx)"
    }
    , 

  "corum":    {
    "desc":"corum"
    }
    ,
  "ctd":    {
    "desc":"ctd"
    }
    ,
  "humancyc":    {
    "desc":"humancyc"
   }
    ,
  "inoh":    {
    "desc":"inoh",
    }
    ,
  "intact_complex":    {
    "desc":"intact_complex"
    }
    ,  
  "kegg":    {
    "desc":"kegg"
    }
    ,
  "mirtarbase":    {
    "desc":"mirtarbase"  
    }
    ,
  "msigdb":    {
    "desc":"msigdb"
    }
    ,
  "netpath":    {
    "desc":"netpath"  
    }
    ,
  "panther":     {
    "desc":"panther"
    }
    ,
  "pathbank":    {
    "desc":"pathbank"
    }
    ,
  "pid":    {
    "desc":"pid" 
    }
    ,
  "psp":    {
    "desc":"psp"
    }
    ,
  "reactome":    {
    "desc":"reactome"
    }
    ,
  "reconx":    {
    "desc":"reconx" 
    }
    
}
    
    
def str2dict(str):
    m=dict()
    #['#Columns:', 'ID', 'DESCRIPTION', 'TYPE', 'HOMEPAGE',
    #'PATHWAYS', 'INTERACTIONS', 'PARTICIPANTS']
    lines=str.split("\n")
    for line in lines:
        row=line.split("\t")
        #print("***-------a------***")
        #print(row) 
        #print("***-------------***")
        if len(row)>2 and row[2]=="BIOPAX":
          mp=dict()  
          
          mp["desc"]=row[1]
          mp["ref"]=row[3]
          mp["pathway"]=row[4]
          mp["interaction"]=row[5]
          mp["participant"]=row[6]
          key=row[0]
          kl=key.split("/")  
          if len(kl)>1:
             key=kl[len(kl)-1]   
          m[key]=mp
          #ar.append(mp)
        #.0['http://pathwaycommons.org/pc12/reactome',
        #"1Reactome v69 (only 'Homo_sapiens.owl') 28-May-2019",
        #'2BIOPAX', 
        #'3https://www.reactome.org',
        #4'2272', '19640', '46217']
  

    #print(m)  
    return m
 
def writeinfo(r,infof):
       with open(infof, "w") as outfile: 
            json.dump(r, outfile) 
       

##############    

def writerows(rows,f):
       with open(f, "w") as outfile: 
            json.dump(rows, outfile) 
  
    
    
def readrows(f):
    obj=None
    with open(f, 'r') as openfile: 
      obj = json.load(openfile) 
    return obj

##############


def wgetinfo(url,infof):
    rs=""
    try:
       response = requests.get(url)
       rs=response.text
       
    except :
       rs=None
    #print(rs) 
    if rs is not None:
       rdi=str2dict(rs)
       with open(infof, "w") as outfile: 
            json.dump(rdi, outfile) 
       
    return  rs

def infofromfile(f,dmap):
    m=dict()
    with open(f, 'r') as openfile: 
      obj = json.load(openfile) 
    for k in dmap.keys():
      m[k]=dmap[k]
    for k in obj.keys():
      m[k]=obj[k]
        
    return m
    

def sizeof_fmt(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def mapinfo(ke,dmap):
    #print(dmap)
    k=ke.lower()
    if k in dmap.keys():
        m= dmap[k]
        return m
    else:
        
        d={
            ke:    {
            "desc":ke,
            "ref":None,
            "srcurl":None        
            } 
          }
        return d
    
def match(label):
    rg = re.compile(r'.*\.(\w+)\.BIOPAX.*') 
    re_match = rg.match(label)
    if re_match is not None:
      #print("****")  
      nl=list(re_match.groups())
      if nl    is not None and len(nl)>=1:
        print(nl)
        ma=nl[0]
        return ma
    return None

 

def name2label(nm,dmap):
    label=nm
    desc=nm
    ma = match(label)
    #print("--%s--%s"%(ma,label))
    if ma is  None:
        for hlb in  hardlabel.keys():
         if hlb in label:
           ma=hlb
    #print("2--%s--%s"%(ma,label))    
    if ma is not None:
        label=ma
        info=mapinfo(label,dmap)
    return label, info

def linestruct(file,gzfile,ncount,ecount,dmap,ecountmap,remap):
   sz=os.stat(gzfile).st_size
   #lastm=time.ctime(os.path.getmtime(file))  
   lastm=time.strftime('%m/%d/%Y', time.gmtime (os.path.getmtime(gzfile))) 
   row= tablelineimpl(dmap,os.path.basename(file),
                        baseurl, sizeof_fmt(sz),
                        lastm,ncount,ecount,ecountmap,remap )
   return row

def tablelineimpl(dmap,nm,burl,sz,lastm,ncount,ecount,ecountmap,remap):
  

  label,info=name2label(nm,dmap)
 #{'desc': 'KEGG 07/2011 (only human, hsa* files), converted to BioPAX by BioModels (http://www.ebi.ac.uk/biomodels-main/) team', 'ref': 'https://www.genome.jp/kegg/',
 #'pathway': '122', 'interaction': '3568', 'participant': '3536'}
  desc=""
  if "desc" in info:
    desc=info["desc"]
  vll={
      "http://www.ebi.ac.uk/biomodels-main/":"http://www.ebi.ac.uk/biomodels"
      }
  #  http://inoh.hgc.jp/ > https://dbarchive.biosciencedbc.jp/en/inoh/desc.html

  for vl in vll.keys():
    desc=desc.replace(vl,vll[vl])
  ref=""
  if "ref" in info:
    ref=info["ref"]

  biopax_info=""
  ils=list()
  lbi=""
  if "biopax_size" in info:
     ils.append(info["biopax_size"])    
  if "interaction" in info:
     
     lbi=" interaction"   
     try:
        intr=int(info["interaction"])   
        if intr>0:
           lbi=lbi+"s"
     except:
        pass
     #ils.append(" "+info["interaction"]+lbi)
  if len(ils)>0:
    if ils[0] != "":
       biopax_info="("+",".join(ils)+")"
        

  srcurl=""
  srcurllab=""
  if "srcurl" in info:
    srcurl=info["srcurl"]      
    srcurllab="BIOPAX"      
  
  reflab="site"
  if ref is None or ref=="":
      reflab=""
      ref="#"
  if srcurl is None or srcurl=="":
      srcurllab=""
      srcurl="#"
    
  tag=['s','p','a','i','m']
  tagl={'s':'substrat','p':'product','a':'activator','i':'inhibitor','m':'modulator'}
  afm=""
  aalt=""
  ecm=dict()
  for t in tag:
    
    if t in  ecountmap.keys():
       v=ecountmap[t]
    else:
       v=0
    ecm[t]=v
    fm="%s=%s," % (t,v)
    alt="%s=%s," % (tagl[t],v)
    afm+=fm
    aalt+=alt
   
 
  nm=nm+".gz"  
  if label=="inoh":
      ref="https://dbarchive.biosciencedbc.jp/en/inoh/desc.html"

  rowm= {
             "label":label.lower(),"desc":desc,"burl":burl,"fname":nm,
             "date":lastm,"size":sz,"ncount":ncount,
             "ecount":ecount,
             "s":ecm['s'],"p":ecm['p'],"a":ecm['a'],"i":ecm['i'],
             "ref":ref,"reflab":reflab,"srcurl":srcurl,
             "srcurllab":srcurllab,"biopax_info":biopax_info,
             "pos":100,"r":remap['reaction'],"e":remap['entity'],
             "test":"test"
          }
    

  return rowm



def listsrc(url,prefix,dmap):  
    urls=list()
    ar=list()
    try:
       response = requests.get(url)
       r=response.text
       #print(r) 
       lst=r.split('<a')
       lst=map(lambda x : x.strip(),lst)  
       #print(lst)
       for l in lst:
         #print("-->"+l)
         urls = re.findall(r'(\w+.BIOPAX.owl.gz)', l)
         szs = re.findall(r'\s(\d+\.?\d*[KMG])', l)

         if urls is not None and len(urls)>0:   
           #print(szs) 
           sz="-"
           if szs is not None and len(szs)>0:   
              sz=szs[0]  
           #print(l)  
           for n in urls:
             if "BIOPAX" in n:
              #print(n+"\n\n") 
              u=url+prefix+n            
              ar.append(u)
              fg=n   
              fg=fg.replace(".gz","")
              fg=fg.replace(".owl","")      
              fg=fg.replace(".BIOPAX","")
              fg=fg.lower()
              #print("--"+fg+"\n\n") 
              lfg=[fg,fg.lower()]
              for fgg in lfg:  
                if fgg in dmap.keys():
                  #print(fg) 
                  #print("!!!%s\n" %fg)
                  dmap[fgg]['srcurl']=u
                  dmap[fgg]['biopax_size']=sz
                    
                

    except Exception as e:
       print("error: "+str(e) )
       r=None
     
    return ar,dmap


def generatePage(template,templatedir,rows,outfile):
    #root = os.path.dirname(os.path.abspath(__file__))
    #templates_dir = os.path.join(root, 'templates')
    #templates_dir = os.path.dirname(os.path.abspath("./"))
    env = Environment( loader = FileSystemLoader(templatedir) )
    template = env.get_template(template)
 
 
    #filename = os.path.join(root, 'html', 'index.html')
    filename = outfile
    with open(filename, 'w') as fh:
        fh.write(template.render(
             
            grows = rows,
            show_table = True,
            sourceinfo = "Input sources from Pathway Commons v 12 (PC): https://www.pathwaycommons.org/archives/PC2/v12/",
            processedinfo = "Data processed with Pax2Graphml.",

        ))    
 

def reaction_key_words():

    reaction_kw=[  'Control',   'BiochemicalReaction',   'ComplexAssembly', 'Catalysis','Conversion','TemplateReactionRegulation','Transport','Degradation','TransportWithBiochemicalReaction','TemplateReaction']
    return reaction_kw

def entity_key_words():

    entity_type=['Protein', 'PhysicalEntity', 'Complex',   'Dna', 'Rna',  'SmallMolecule','RnaRegion','DnaRegion' ]
    return entity_type


def count_reaction_and_entity(gr):        
 
  reaction_kw=reaction_key_words()

  entity_type=entity_key_words()


  btl=p2g.properties.node_property_values(gr, "biopaxType")
  for bt in btl:
     found=0
     if bt in reaction_kw:
          found=1
     if bt in entity_type:
          found=2
     if found==0:       
        print(" warning biopaxType %s not referenced in reaction or entity" %bt)  
  remap=dict()
  remap["reaction"]=0
  remap["entity"]=0
  count_map=p2g.properties.count_nodes_by_values(gr,  "biopaxType")
  for bt in reaction_kw:
    if bt in count_map.keys():
          remap["reaction"]=remap["reaction"]+count_map[bt]  
  for bt in entity_type:
    if bt in count_map.keys():
          remap["entity"]=remap["entity"]+count_map[bt]  

  return remap          
            
def formatn(amount):
    amount="%s" %(amount)
     
    orig = amount
    nw = re.sub("^(-?\d+)(\d{3})", '\g<1>,\g<2>', amount)
    if orig == nw:
        return nw
    else:
        return formatn(nw)


########################################################
##############main  


 
rows=list()
if processmode==1:    
  if doextractinfo==True:
     wgetinfo(urlinfo,infofile)
     dmap=infofromfile(infofile,dmap)       
     urls,dmap=listsrc(srcurl,prefix,dmap)
     writeinfo(dmap,infofile)    
  
  dmap=infofromfile(infofile,dmap)        
  #print(dmap)
  graphmlfiles = glob.glob(datadir+"/"+'*')
  print(graphmlfiles)  

  gtablerow=""
  for file in   graphmlfiles:
     print(file)
     file_name = os.path.basename(file ) 
     gzfile=datadirgz+"/"+file_name+'.gz'
     print(gzfile)
     ma = match(file)
     if ma is  None:
       for hlb in  hardlabel.keys():
         if hlb in file:
           ma=hlb
     if ma is not None:   
       g=p2g.graph_explore.load_graphml(file, directed=True)
       nodes=p2g.node_list(g)
       edges=p2g.edge_list(g)
       print("nodes count: %s edges count: %s "  %(len(nodes),len(edges)  )   )    
 
       if len(nodes)>0:
         ecountmap=p2g.properties.count_edges_by_values(g,"spaim")
         remap=count_reaction_and_entity(g)
         print(remap)        
         row=linestruct(file,gzfile,len(nodes),len(edges), dmap,ecountmap,remap )
         rows.append(row)
         #print(row)
         #gtablerow+=row+"\n\n"
  
  for row in rows:
      k=row["label"]  
      for hlb in  hardlabel.keys():
          if k in hlb:
              row["pos"]=hardlabel[hlb]["pos"]
                
          
  print("--------")  
  rows = sorted(rows, key=lambda k: k['label'], reverse=False)          
  rows = sorted(rows, key=lambda k: k['pos'])
  #rowfile
  print("--------")  
  print(rows)  
  generatePage('template.html',templatedir,rows,"db.html")
  generatePage('template.tex',templatedir,rows,"table.tex")
  writerows(rows,rowfile)
 
if processmode==2:       

   rowst=readrows(rowfile)
   rows=list()
   for row in rowst:
        
        row['r']=formatn(row['r'])
        row['e']=formatn(row['e'])
        row['s']=formatn(row['s'])
        row['p']=formatn(row['p'])
        row['a']=formatn(row['a'])
        row['i']=formatn(row['i'])
        row['ecount']=formatn(row['ecount'])
        row['ncount']=formatn(row['ncount'])
        rows.append(row)

   generatePage('template.html',templatedir,rows,"db.html")
   generatePage('template.tex',templatedir,rows,"table.tex")



