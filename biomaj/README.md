
#### BIOMAJ processes & Singularity

These section contains configurations and 

scripts to generate the [GraphML data banks](https://pax2graphml.genouest.org/db.html) from large BIOPAX files

managed by  [BIOMAJ](https://biomaj.genouest.org/)

PAX2GRAPHML is launched within a container executed with [Singularity](https://sylabs.io/singularity/) on a computer cluster.

Remark : For very large file, An extended amount of CPU and RAM can be necessary for BIOPAX file processing.

```
#Singularity command line example:

singularity run -B /db:/db docker://fjrmore/pax2graphml  python3  spaim.py $resultPath $biopaxfile

```
