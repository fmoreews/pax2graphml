#!/bin/bash

. /softs/local/env/envsingularity-3.5.2.sh

IMAGENAME=fjrmore/pax2graphml

WORKDIR=$1
WORKFILE=$2

singularity run -B /db:/db docker://fjrmore/pax2graphml python3 /db/biomaj/biomaj-process/pax2graphml/filter.py $WORKDIR $WORKFILE 
