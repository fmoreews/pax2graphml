### Linux installation (Ubuntu and Debian)
We describe here how to install pax2graphml with all dependencies, graph-tool, jupyter lab and  java
please note that this installation can be tricky. 

For the  docker or conda  installations (recommanded), see the [main page](https://gitlab.inria.fr/fmoreews/pax2graphml/-/blob/master/docker/pax2graphml/README.md) 
 
 * For the classical installation (from sources), 

open a terminal and enter the following commands:



```
#Tested on Ubuntu 20.04 
echo "TUSER=$USER"
sudo -i
TUSER=myuser # put you user name here like displayed by the echo command
apt-get update
 
DISTRI=focal &&\
    DEBIAN_FRONTEND=noninteractive &&\
    TZ=America/New_York &&\
    WORKD=/home/$TUSER &&\
    PDIR=/home/$TUSER &&\
    PATH=$PATH:/home/$TUSER/.local/bin

apt-get remove -y gnupg &&    apt-get install -y gnupg2 dirmngr ca-certificates
 
echo "deb [ trusted=yes] https://downloads.skewed.de/apt $DISTRI main">>  /etc/apt/sources.list;
apt-get update --allow-unauthenticated
apt-get install -y  --no-install-recommends \
   mlocate sudo nano wget curl  xvfb \
   vim openjdk-8-jre 

apt-get install -y python3-pip python3-cairo \
 python3-gi-cairo tzdata libgtk3.0 libgtk-3-dev zip unzip  

apt-get install -y  python3-graph-tool

pip3 install --upgrade pip && pip3 install virtualenv
pip3 install pandas==1.2.3 && pip3 install matplotlib==3.3.4
pip3 install lxml==4.6.2 && pip3 install kmapper==1.2.0
pip3 install subprocrunner==1.2.1 && pip3 install pybiomart==0.2.0

pip3 install sphinx && pip3 install -U pip setuptools twine 

pip3 install jupyterlab==2.2.9
 
cd $WORKD

jupyter kernelspec list 



SRC=$WORKD/pax2graphml/src &&\ 
  USRBIN=/usr/bin/pax2graphml &&\
  LPAGE=/usr/local/lib/python3.8/dist-packages/notebook/templates 


mkdir -p $USRBIN && chmod -R 777 $USRBIN

 


git clone https://gitlab.inria.fr/fmoreews/pax2graphml.git
 
mkdir -p  /home/$TUSER/.jupyter/
cp ./pax2graphml/docker/pax2graphml/conf/*  /home/$TUSER/.jupyter/
sudo -i
cp -r $SRC/script $USRBIN/script && \
       cp -r $SRC/test $USRBIN/test 



sed -i 's/silent=False/silent=True/g' $SRC/pax2graphml/__init__.py &&\
    chmod -R 755 $USRBIN  && cd $SRC/ && bash install.sh && \
    python3   -c "import pax2graphml  as p2g; print('package p2g installed')"  

cp -r $WORKD/pax2graphml/docker/pax2graphml/entrypoint /usr/bin/entrypoint

chmod +x /usr/bin/entrypoint/* && chmod -R 777 $WORKD && rm -rf $SRC && \
  chown -R $TUSER $LPAGE && chmod 777 -R $LPAGE  \
 && mkdir -p /home/$TUSER/example   && chmod -R 777 /home/$TUSER/example


sed -i "s/home\/user/home\/$TUSER/g" /home/$TUSER/.jupyter/jupyter_notebook_config.py




exit
su $TUSER

 

```

 * Test using demo Jupyter + dataset
```
PATH=$PATH:/usr/bin/entrypoint/
SHOW_PWD=true
JUPYTER_PWD=1234
##################
demo.sh
 
##################
 
#or 
echo "downloading example dataset"
curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/docker/pax2graphml/data.zip -o data.zip
 
unzip data.zip && rm data.zip
mv data/* /home/$USER/example/

find /home/$USER/example/ -name "*.ipynb" -exec sed -i "s/home\/user/home\/$USER/g" '{}' \;

echo "running jupyter lab"
jupyter.sh



```

 * Test using scripting

```
python3   -c "import pax2graphml  as p2g; print('package p2g installed')"  

curl https://gitlab.inria.fr/fmoreews/pax2graphml/-/raw/master/test/test.py -o test.py
python3 test.py

```







