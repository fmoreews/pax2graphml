### Install pax2graphml Virtual Machine

This instructions allow to use pax2graphml  on Windows, Linux and MacOs using a Virtual Machine (VM)

* first install [virtualbox](https://www.virtualbox.org)

* then download the pax2graphml OVA file (7Go) :

[https://pax2graphml.genouest.org/ova/pax2graphml_jupyter_ubuntu_20.04.ova](https://pax2graphml.genouest.org/ova/pax2graphml_jupyter_ubuntu_20.04.ova)
or [drive link](https://drive.google.com/file/d/1ccGdFjpExH8bpvpoG0IliryEBk05nOvf/view?usp=sharing)
or [alternate link](https://data-access.cesgo.org/index.php/s/WaZJg03u5ZQ1DzH)

* install the VM from the OVA file (double click on file and follow instructions)

* start the VM within virtualbox (better with internet connection)

* login with login *ubuntu*, password *ubuntu*


* the jupyter lab pax2graphml session is then automaticallly launched (url: http://localhost:80/lab? )

* enter the displayed password


* click and the example folder and test the existing python codes


remark: 
for keyboad issue (qwerty vs azerty ...) use the following command in a terminal within the VM session: 
```
setxkbmap fr  # azerty
#or 
setxkbmap en  # qwerty
```
